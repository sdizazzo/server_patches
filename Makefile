JAVAC = javac
CLASSPATH = -cp game.jar:out:$(shell find third_party/libs -name "*.jar" | tr '\n' ':')

_build:
	@echo Copying game.jar
	@cp third_party/game.jar ./
	@echo Building Java classes...
	@rm -r ./out
	@mkdir -p ./out
	@$(JAVAC) $(CLASSPATH) -d "out" $(shell find src -name "*.java")
	@echo Adding patches to game.jar
	@cd out; jar uf ../game.jar ./*
	@echo Complete.

clean:
	rm -r "out" "game.jar"
