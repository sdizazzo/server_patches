# Server Patches

A collection of small patches for the Altitude server software.

## Releases

see [Tags](https://gitlab.com/alti2d/server_patches/tags)

## Features

 - The 'nickname bug', which prevented nickname changes from being shown in the
   JSON log on rare occasions has been fixed.

 - Includes a workaround for the server config size limit, allowing a virtually
   unlimited number of maps and commands.

 - A few new commands (listed below).

 - Bots can be created and removed dynamically, and their plane setup controlled
   per-bot.

 - An optional custom permissions system for commands with non-hierarchical
   permission groups.

 - Configurations, including permissions, can be reloaded without restarting the server.

 - Join behaviour can (optionally) be specified programmatically. Two join handlers
   are included by default, a level restriction which takes ace ranks into account,
   and a simple whitelist. More can be added easily.

 - Some JSON log events have extra fields, and some new events are added.

 - Significantly reduced `commands.txt` latency.

 - Bot names can be specified in the configuration.

 - Team chat can optionally use tournament teams rather than current teams.

## Usage

A modified game.jar is included in the `release` directory of this repository.
To use the patches, simply overwrite your existing game.jar with this file, and
copy the two configuration files to altitude's `servers` directory.

```
   $ cp release/game.jar /path/to/altitude/
   $ cp mods.hjson permissions.hjson /path/to/altitude/servers/
```

`mods.hjson` contains configuration for all of the changes made by this set of patches.
The `permissions.hjson` file can be omitted if you're not enabling the custom
permissions system (see the comments in `mods.hjson`).

### Compilation

Patches can be built using the included makefile. `./game.jar` is created automatically,
by adding the modified class files to an unmodified game.jar (`third_party/game.jar`)

```
   $ make
```

### Commands

Only commands in the `client_commands` array in the global config are sent to clients,
any not specified can only be executed through `commands.txt`

- `/reloadModConfig` - Reloads `servers/mods.hjson`.

- `/reloadPermissions` - Reloads permissions from the file specified in
  the main configuration file.

- `/setTime <ticks>` - overwrites the elapsed time for timed games.
  The single parameter is an integer number of ticks, where there are 30 ticks
  in a second. e.g. In a game with 7 minute rounds, `setTime 0` will set the
  elapsed time to 0, so the remaining time will be 7:00, `setTime 12600` will
  end the round.

- `/setRoundTime <ticks>` - overwrites the elapsed time for a single round of a
  game. This only works for the 1de, 1dm, and 1bd game modes.

- `/endRound` - ends the current round instantly, displaying the score and awards.

- `/getTime` - get the current elapsed time for timed games, prints an entry to the
  JSON log. `roundTimeTicks` gives the elapsed time of the current round, and is
  only set for 1de, 1dm and 1bd. `gameTimeTicks` gives the elapsed time for the entire
  game, and is set for any game mode which is timed.

  ```
  {"roundTimeTicks":12481,"gameTimeTicks":12481,"port":27276,"time":23434,"type":"getTime"}
  ```

- `/botPlaneSetup <bot> <plane> <red> <green> <blue> <skin>` - Sets the plane setup for bots
  to use. All the values are integers, perks are 0 to 3 in the order shown on
  the plane selection screen, as are planes: 0 = loopy, 1 = bomber, 2 = explodet,
  3 = biplane, 4 = miranda.
  `bot` is the number of the bot to change (e.g. "Bot 1" is bot number 1),
  `0` to set the setup for all existing bots, or `-1` to set the default for
  newly created bots.

- `/botPlaneRandom <bot>` - Sets the plane setup for one or all bots to be fully random.
  `bot` is the number of the bot to change (e.g. "Bot 1" is bot number 1),
  `0` to set the setup for all existing bots, or `-1` to set the default for
  newly created bots.

 - `/addBot <difficulty>` - Adds a new bot to the server. `difficulty` is a value from
  0 to 3 `(0 => easy, 1 => medium, 2 => hard, 3 => expert).` The bot's number will
  be the lowest available.

 - `/removeBot` - Remove the most recently added bot (that with the highest number)

 - `/overrideTdmScore <score> <score>` - Sets the current score in a TDM game, similar
  to the `/overrideBallScore` command but for TDM maps.

 - `/applyForce <playerID> <x> <y>` - Apply the given force to a player. Values are
  limited to +-15.

 - `/resetBall <x> <y>` - Reset the current round in ball, re-spawning all players,
  and spawning the ball at the given position.

 - `/resetBallTeam <Team>` - Reset the current round in ball, re-spawning all players,
  and spawning the ball at it's spawn point for the given team (-1 -> Neutral, 0 -> Left team,
  1 -> Right team).

 - `/allowSpectatorAllChat <allow>` - When `allow` is false, any messages from
  spectators in all chat are redirected to team chat. When a tournament is running,
  players in the tournament can still use all chat while spectating.
  Running the command again with `allow` as true reverts the restrictions.
  Changing this value persists over map changes, but not over server restarts.

 - `/denySpawn <playerID> <reason>` - Prevent the specified player from spawning.
  This applies until the player disconnects or /allowSpawn is called. Spawning
  can be disabled by default in `mods.hjson`.

 - `/allowSpawn <playerID>` - Allow the specified player the ability to spawn.

#### Perk blocking

Commands have been added to dynamically block/allow specific perks or random
modes. Exceptions to individual blocks can be made for specific players -- these
exceptions are automatically reset at the end of every map.

When any perk is blocked random modes are disabled. Per player exceptions can
be added for this.

Note that when explicitly blocking full random, bots can't spawn unless you
overwrite their setup with `/botPlaneSetup`.

A list of valid perk names is given below the commands.

##### Commands

 - `blockPerk <perkName> <reason>` Blocks the perk with the given name, when a
  player attempts to spawn using this perk they'll be sent the `<reason>`.

 - `unblockPerk <perkName>` Removes the block for the given perk.

 - `allowPlayerPerk <perkName> <playerNo>` Adds an exception to allow the given
  player to spawn with a blocked perk. Has no effect if the perk isn't blocked.

 - `resetPlayerPerk <perkName> <playerNo>` Removes an exception.

 - `resetPerks` Resets the list of blocked perks an exceptions.

##### Perks

Any of the following names can be given for the `<perkName>` field in the above
commands. Names are case-sensitive.

- "Full Random"
- "Config Random"
- "Tracker"
- "Double Fire"
- "Acid Bomb"
- "Suppressor"
- "Bombs"
- "Flak Tailgun"
- "Director"
- "Thermobarics"
- "Remote Mine"
- "Dogfighter"
- "Recoilless Gun"
- "Heavy Cannon"
- "Trickster"
- "Laser"
- "Time Anchor"
- "Rubberized Hull"
- "Heavy Armor"
- "Repair Drone"
- "Flexible Wings"
- "Turbocharger"
- "Ultracapacitor"
- "Reverse Thrust"
- "Ace Instincts"

## `/assignTeam`

The `assignTeam` command has been modified to also work for ffa. Assigning a
player to their own team forces them to de-spawn, as usual.


| Team number | Team name |
| ----------- | --------- |
| -1          | Spectator |
|  3          | Red       |
|  4          | Blue      |
|  5          | Green     |
|  6          | Yellow    |
|  7          | Orange    |
|  8          | Purple    |
|  9          | Azure     |
|  10         | Pink      |
|  11         | Brown     |


To change this behaviour, set `ffa_allow_assign_team` in `mods.hjson`.


## Permissions

A global permission system is included, which works with all commands (the defaults,
those added by patches, and external commands added in `custom_json_command.txt`)

A set of groups are defined, each containing any number of users identified by
their vapor IDs.

Each command then has a list of groups which can use that command, with
the special group names 'all' and 'vote' being used to allow anyone to use the
command, or anyone to call a vote for it, respectively.


## Extra logs

Extra information is added to some JSON log events.

 - `positionX` and `positionY` fields added to `structureDestroy` and
   `structureDamage` events for turrets and bases.

 - `playerVelX` and `playerVelY` fields added to `powerupPickup` and
   `powerupAutoUse` events.

 - `stats` and `lastPos` added to the `clientRemove` event

 - various fields added to `clientAdd`: `cp_clicks`, `cp_admin`, `cp_facebook`,
   `cp_refer`, `cp_gift`, `cp_steam`, `loggedInTimeMs`, `totalAceKills` `totalAceDeaths`

 - `aceRanks` `levels` and `loggedInTimes` added to `logServerStatus`, giving
    the data in the same format as in `clientAdd`.

When the server launcher is started, the server\_patches version is sent to the
JSON log (the current version is shown at the top of this readme):

```
{"version":"0.3.1","port":-1,"time":6,"type":"serverPatches"}
```

Some example events:
```
// Damaging a turret
{"positionY":57,"port":27276,"exactXp":1.3229169845581055,"xp":1,"time":79661"
 "type":"structureDamage","player":1,"target":"turret","positionX":2955}

// Destroying a turret
{"positionY":57,"port":27276,"xp":10,"time":79860,"type":"structureDestroy",
 "player":1,"target":"turret","positionX":2955}

// Picking up a powerup
{"powerup":"Bomb","positionY":658,"playerVelX":7.12,"playerVelY":7.37,
 "port":27276,"velocityX":0,"time":231519,"type":"powerupPickup","velocityY":0,
 "player":1,"positionX":405}

// Auto-using a health powerup
{"powerup":"Health","positionY":455,"playerVelX":4.93,"playerVelY":1.53,
 "port":27276,"velocityX":0,"time":322157,"type":"powerupAutoUse","velocityY":0,
 "player":1,"positionX":1952}
```

### `despawn`

Stats are tracked per-spawn as well as per-round. When a player de-spawns (disconnects,
a goal is scored, round ends, assigned to a team, etc..) their stats and perks are sent
to the JSON log:

```
{"plane":"Loopy","stats":{"Crashes":0,"Kills":0,"Longest Life":0,"Ball Possession Time":0,"Goals Scored":0,"Damage Received":100.89474,"Assists":0,"Experience":35,"Damage Dealt":63.05085,"Goals Assisted":0,"Damage Dealt to Enemy Buildings":1765.416,"Deaths":1,"Multikill":0,"Kill Streak":0},"port":27276,"perkGreen":"Repair Drone","perkRed":"Acid Bomb","skin":"No Skin","team":3,"time":280565,"type":"despawn","perkBlue":"Ace Instincts","player":0}
```

### `turretEmp`

A new JSON log message is sent when a player EMPs a turret:

```
{"port":27276,"time":93779,"player":1,"type":"turretEmp"}
```
### `clientRemove`

clientRemove now reports the last known position reported by the client in `lastPos`:

```
{"vaporId":"00000000-0000-0000-0000-000000000000","reason":"Client left.","stats":{"Crashes":0,"Kills":0,"Longest Life":6,"Ball Possession Time":33,"Goals Scored":0,"Damage Received":100.421936,"Assists":0,"Experience":0,"Damage Dealt":0,"Goals Assisted":0,"Damage Dealt to Enemy Buildings":0,"Deaths":1,"Multikill":0,"Kill Streak":0},"port":27276,"ip":"10.0.0.106:27272","nickname":"wild bum!","time":2652918,"type":"clientRemove","message":"left","player":1,"lastPos":{"x":2276,"y":603,"angle":-1}}
```
If there is no know position the following will be reported:
```
{ ... ,"lastPos":{}}
```

### `playerInfoEv`

A new JSON log message has been added for tracking player info events, which are
sent by a client when they change plane setup, change team, level up, change ace,
or leave the server.

Some examples:

```
// Joining the server
{"plane":"Loopy","ace":4,"level":60,"port":27276,"perkGreen":"Heavy Armor",
 "perkRed":"Double Fire","team":2,"time":435644,"type":"playerInfoEv",
 "leaving":false,"perkBlue":"Turbocharger","player":1}

// Joining Red team
{"plane":"Loopy","ace":4,"level":60,"port":27276,"perkGreen":"Heavy Armor",
"perkRed":"Double Fire","team":3,"time":486878,"type":"playerInfoEv",
"leaving":false,"perkBlue":"Turbocharger","player":1}

// Switching planes (without spawning)
{"plane":"Explodet","ace":4,"level":60,"port":27276,"perkGreen":"Flexible Wings",
"perkRed":"Thermobarics","team":3,"time":532207,"type":"playerInfoEv",
"leaving":false,"perkBlue":"Turbocharger","player":1}

// Leaving the server
{"plane":"Loopy","ace":0,"level":1,"port":27276,"perkGreen":"No Green Perk",
"perkRed":"Tracker","team":2,"time":576477,"type":"playerInfoEv",
"leaving":true,"perkBlue":"No Blue Perk","player":1}
```

There's a lot of overlap with existing events - but without introducing more
state it would be difficult to tell from within the event whether it's a duplicate.
The original use case is tracking level ups for a tutorial server ^^


## Custom join

`CustomJoin.checkJoin`
is called whenever the server receives a join request, it returns null if
the player should be allowed to join, or the message to be sent to the player
if not.

The vapor validation checks (making sure the player is actually who they
claim to be) are not skipped.

The result of the original checker (which will fail if the user is banned,
the password is incorrect, or they don't meet the
level requirements) is passed as the last parameter to `checkJoin`, and can be
ignored by setting the relevant option in `mods.hjson`

A couple of simple examples are included in the default `CustomJoin.java`, which
can be switched between by commenting/un-commenting the relevant lines :

#### whitelist(...)

A simple vapor ID based whitelist - reads a file called `whitelist_[port]`
(where `[port]` is replaced by the port of the server),
line by line, if the vapor ID of the player who requested to join is
not in this file they won't be able to join the server. If a file named
`allow_all_[port]` exists, all players will be allowed to join the server.

#### restrict\_level(...)

An (untested) level restriction which supports restricting aces. Reads a file
`restrict_level_<port>` in the format `ace:level - ace:level` (e.g. 0:0 - 0:30),
and only allows players to join if they're inside this range.
