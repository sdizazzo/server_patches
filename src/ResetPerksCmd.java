class ResetPerksCmd extends Command {

   public ResetPerksCmd(fe game) {
      super(game, "resetPerks", PermissionGroup.admin);
      this.setParams(new JI[]{});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().l().blockedPerks.clear();
      this.getServer().l().allowedPerks.clear();
   }
}
