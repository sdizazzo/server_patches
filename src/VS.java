import mods.Config;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

// - added minimal config packet.
// - automatic sending of player position events.
// - add `canSpawn` map for `allowSpawn` command
public class VS extends wr {

   private float positionLogCounter = 0.0f;
   private Ui confMinimal = null;
   private boolean useMinimalConf = false;

   private static final Logger logger = Logger.getLogger(VS.class);
   private final fN c;
   public HashMap<UUID, Boolean> canSpawn = new HashMap<>();
   public HashMap<UUID, String> spawnDenyReason = new HashMap<>();
   public HashMap<String, String> blockedPerks = new HashMap<>();
   public HashMap<UUID, HashSet<String>> allowedPerks = new HashMap<>();
   private fe d;
   private Wv e;
   private Map f;
   private List g;
   private rG h = new rG(30000.0F, 0, 254);
   private Map i = new HashMap();
   private Map j;
   private String k;
   private qm l;
   private int m;
   private int n = 0;
   public static fe a;
   private Ui o;
   private Map p = new HashMap();
   private static final int q = Math.round(sx.d(5.0F).a());
   private int pingSummaryCounter = 0;
   private static final int s = Math.round(sx.d(1.0F).a());
   private int t;
   private int u;
   private int v;
   private Comparator w;
   private Map x;
   private boolean y;
   private Map z;
   private int A;

   public VS(fN var1, hp var2) {
      this.t = s;
      this.v = 0;
      this.w = new zg(this);
      this.x = new HashMap();
      this.c = var1;
      this.d = new fe(var2.b());
      this.d.ao().a(var2);
      this.d.d();
      a = this.d;
      this.e = this.d.ab().m();
      this.f = new HashMap();
      this.g = new ArrayList();
      this.j = new HashMap();
      if (var2.V() && !this.d.E()) {
         HashSet var3 = new HashSet();
         var3.addAll(var2.k());
         var3.addAll(var2.i());
         Set var4 = a((Set) var3);
         a(var2, var4);
      }

      a(var2);
      this.useMinimalConf = Config.get(port()).config_size_workaround;
   }

   public void a() {
      String var1 = (String) this.c.m().i().get(0);
      this.b();
      this.c(var1);
   }

   public static Set a(Set var0) {
      File var1 = new File(Uc.b, "server-unpacked-maps.xml");
      sR var2 = new sR(var1);
      Map var3 = var2.a("snapshots", new gP(), uy.a, new HashMap());
      HashSet var4 = new HashSet();
      Iterator var6 = var0.iterator();

      while (var6.hasNext()) {
         String var5 = (String) var6.next();

         try {
            String var7 = var5 + "." + "altx";
            File var8 = new File(fe.f, var7);
            if (!var8.exists()) {
               logger.error("Map file \'" + zX.d(var8) + "\' does not exist.");
               var4.add(var5);
            } else {
               uy var9 = (uy) var3.get(var8);
               uy var10 = new uy(var8);
               if (!var10.equals(var9)) {
                  qm var11 = qm.a(fe.f, var7);
                  Uc.a(var11);
                  var3.put(var8, var10);
               }
            }
         } catch (Throwable var12) {
            logger.error(var12, var12);
            var4.add(var5);
         }
      }

      LH var13 = new LH(var1);
      var13.a("snapshots", var3, new gP(), uy.a);
      var13.a();
      return var4;
   }

   public static void a(hp var0, Set var1) {
      List var2 = var0.k();
      Object var3 = var0.i();
      var2.removeAll(var1);
      ((List) var3).removeAll(var1);
      if (((List) var3).size() == 0) {
         var3 = new ArrayList(var2);
      } else {
         Iterator var5 = ((List) var3).iterator();

         while (var5.hasNext()) {
            String var4 = (String) var5.next();
            if (!var2.contains(var4)) {
               var2.add(var4);
            }
         }
      }

      var0.b(var2);
      var0.a((List) var3);
   }

   public static void a(hp var0) {
      int var1 = var0.n();
      int var2 = var0.o();
      if (var1 < 0) {
         var1 = 0;
      }

      if (var2 < 0) {
         var2 = 0;
      }

      if (var1 != 0 && var2 != 0 && var1 > var2) {
         int var3 = var1;
         var1 = var2;
         var2 = var3;
      }

      var0.d(var1);
      var0.e(var2);
   }

   public void a(String var1) {
      DT var2 = new DT(this.d, -1, false, var1);
      Iterator var4 = this.g.iterator();

      while (var4.hasNext()) {
         Jt var3 = (Jt) var4.next();
         var3.a((kY) var2);
      }

   }

   public void b() {
      this.o = new Ui(this.d, this, false);
      if (this.useMinimalConf) {
         this.confMinimal = new Ui(this.d, this, true);
      }
      this.b((Jt) null, this.o);
      Iterator var2 = this.g.iterator();

      while (var2.hasNext()) {
         Jt var1 = (Jt) var2.next();
         var1.b(this.o);
      }
   }

   public fN d() {
      return this.c;
   }

   public void a(DZ var1, LG var2) {
      try {
         Jt var3 = (Jt) this.f.get(var1);
         if (var3 != null) {
            int var4 = var2.b(2);
            if (var4 == this.n) {
               try {
                  this.e.a(var3);
                  var3.a((LG) var2);
               } catch (kW var10) {
                  logger.error("Connection lost due to packet loss", var10);
                  this.a(var3, "Connection lost due to packet loss", "connection lost");
               } finally {
                  this.e.a((Jt) null);
               }
            }
         }
      } catch (Exception var12) {
         logger.error("Error processing packet from " + var1 + " -> " + this.f.get(var1), var12);
      }

   }

   public boolean a(DZ var1) {
      return this.f.containsKey(var1);
   }

   public boolean a(UUID var1) {
      return this.b(var1) != null;
   }

   public Jt b(UUID var1) {
      Iterator var3 = this.f.values().iterator();

      while (var3.hasNext()) {
         Jt var2 = (Jt) var3.next();
         if (var2.f().D().equals(var1)) {
            return var2;
         }
      }

      return null;
   }

   public Jt a(int var1) {
      Iterator var3 = this.f.values().iterator();

      while (var3.hasNext()) {
         Jt var2 = (Jt) var3.next();
         if (var2.g() == var1) {
            return var2;
         }
      }

      return null;
   }

   public String e() {
      return this.k;
   }

   public Collection f() {
      return this.f.values();
   }

   public int g() {
      int var1 = 0;
      Iterator var3 = this.f().iterator();

      while (var3.hasNext()) {
         Jt var2 = (Jt) var3.next();
         if (!var2.m()) {
            ++var1;
         }
      }

      return var1;
   }

   public List h() {
      return this.g;
   }

   public int a(float var1) {
      int var2 = this.d.Y();
      this.d.a(var1);
      int var3 = this.d.Y();
      int var4 = var3 - var2;
      if (var4 > 0) {
         this.v();
      }

      for (int var5 = 0; var5 < var4; ++var5) {
         this.w();
      }

      this.h.a(var1);
      Iterator var6 = this.f().iterator();

      while (var6.hasNext()) {
         Jt var7 = (Jt) var6.next();
         var7.a(var1);
      }

      return var4;
   }

   protected void a(lY id, DC var2, b level) {
      int var4 = this.h.a();
      DZ var5 = id.b();
      logger.info("Adding client \'" + id.E() + "\' from " + var5 + ", playerId=" + var4);
      if (this.f.containsKey(var5)) {
         logger.error("add client failed -- " + this.f.get(var5) + " is already connected from " + var5);
      } else {
         this.x.remove(Integer.valueOf(var4));
         Jt client = new Jt(this, id, var2, var4, level);

         try {
            da log = JL.a("clientAdd");
            log.a("player", var4);
            log.a("nickname",  id.E());
            log.a("ip",  var5.toString());
            log.a("vaporId",  id.D().toString());
            log.a("aceRank", level.a());
            log.a("level", level.b());
            log.a("demo", !client.o());
            log.a("cp_clicks", id.i());
            log.a("cp_admin", id.m());
            log.a("cp_facebook", id.n());
            log.a("cp_refer", id.o());
            log.a("cp_gift", id.p());
            log.a("cp_steam", id.j());
            log.a("loggedInTimeMs", id.g());
            log.a("totalAceKills", level.c());
            log.a("totalAceDeaths", level.d());
            this.c.a(log);
         } catch (Exception var14) {
            logger.error(var14, var14);
         }

         if (!client.m()) {
            Collection var15 = this.f();
            ArrayList var8 = new ArrayList(this.j());
            Iterator var10 = var8.iterator();

            label57:
            while (true) {
               Jt var9;
               boolean var12;
               boolean var13;
               do {
                  boolean var11;
                  do {
                     if (!var10.hasNext()) {
                        break label57;
                     }

                     var9 = (Jt) var10.next();
                     var11 = !var15.contains(var9);
                  } while (!var11);

                  var12 = ty.a((Object) var9.f().D(), (Object) client.f().D());
                  var13 = ty.a((Object) var9.f().E(), (Object) client.f().E());
               } while (!var12 && !var13);

               this.i.remove(Integer.valueOf(var9.g()));
            }
         }

         this.i.put(Integer.valueOf(var4), client);
         this.a(client, this.l);
         this.f.put(var5, client);
         Iterator var17 = this.h.b().iterator();

         while (var17.hasNext()) {
            Integer var16 = (Integer) var17.next();
            if (this.i.containsKey(var16)) {
               this.i.remove(var16);
            }
         }
      }

   }

   public Collection j() {
      return this.i.values();
   }

   public Jt b(int var1) {
      return (Jt) this.i.get(Integer.valueOf(var1));
   }

   public void a(Jt var1, String var2, DC var3) {
      String var4 = var1.f().E();
      if (!ty.a((Object) var4, (Object) var2)) {
         logger.info("[Nickname Changed] \'" + var4 + "\' -> \'" + var2 + "\'");

         try {
            da var6 = JL.a("clientNicknameChange");
            var6.a("player", var1.g());
            var6.a("oldNickname", (Object) var4);
            var6.a("newNickname", (Object) var2);
            var6.a("ip", (Object) var1.e().toString());
            var6.a("vaporId", (Object) var1.f().D().toString());
            this.c.a(var6);
         } catch (Exception var7) {
            logger.error(var7, var7);
         }

         var1.f().b(var2);
         mF var5 = var1.h();
         var5.e().a().b(var2);
         this.c(var1, new Bi(this.d, true, var1.g(), var5.e()));
      }

      var1.a(var3);
   }

   public Jt b(DZ var1) {
      return (Jt) this.f.get(var1);
   }

   public void a(Jt var1, String var2, String var3) {
      if (!this.f.containsKey(var1.e())) {
         logger.warn("Unnecessary removeClient(" + var1 + ") - user is not connected");
      } else {
         var1 = (Jt) this.f.remove(var1.e());
         logger.info("Removing client \'" + var1.f().E() + "\' from " + var1.f().b() + ", playerId=" + var1.g() + ", message: " + var2);

         dp player_cont = this.n().J().u();
         mF player = player_cont.c(var1.g());
         xs stats = player.g();

         canSpawn.remove(player.e().a().D());
         spawnDenyReason.remove(player.e().a().D());

         // Create a map of stat names to values.
         HashMap<String, Number> stat_map = new HashMap<>();

         for (Object a : stats.r) {
            SX stat = (SX) a;
            stat_map.put(stat.b(), stat.a());
         }

         HashMap<String, Number> lastPos = new LinkedHashMap();
         if(player.i() != null) {
            lastPos.put("x", player.i().getLastX());
            lastPos.put("y", player.i().getLastY());
            lastPos.put("angle", player.i().getLastAngle());
         }

         try {
            da var4 = JL.a("clientRemove");
            var4.a("player", var1.g());
            var4.a("nickname", (Object) var1.f().E());
            var4.a("stats", stat_map);
            var4.a("ip", (Object) var1.e().toString());
            var4.a("vaporId", (Object) var1.f().D().toString());
            var4.a("reason", (Object) (var2 == null ? "Client left." : var2));
            var4.a("message", (Object) (var3 == null ? "left" : var3));
            var4.a("lastPos", lastPos);
            this.c.a(var4);
         } catch (Exception var6) {
            logger.error(var6, var6);
         }

         this.x.remove(Integer.valueOf(var1.g()));
         this.h.a(var1.g());
         this.g.remove(var1);
         this.c.i().a(var1.e());
         Or var7 = new Or(new aaf(var1.f()));
         Bi var5 = new Bi(this.d, false, var1.g(), var7);
         var5.a(var3);
         this.c(var1, var5);
         if (var2 != null) {
            this.c.j().a(var1.e(), var2);
         }

      }
   }

   public void a(Jt var1, String var2, long var3, boolean var5, boolean var6) {
      if (var1 == null) {
         logger.warn("Invalid ban target: null");
      } else {
         long var7 = System.currentTimeMillis();
         long var9 = var7 + var3;
         byte[] var11 = var1.f().b().i();
         UUID var12 = var1.f().D();
         String var13 = var1.f().E();
         qo var14 = this.c.m().R();
         if (var5) {
            eF var15;
            if (var6) {
               var15 = var14.a(var11);
               if (var15 != null && var15.d() > var9) {
                  logger.info("Updating previous IP ban of longer duration");
                  var2 = var15.f();
                  var9 = var15.d();
               }
            }

            var15 = var14.a(var12);
            if (var15 != null && var15.d() > var9) {
               logger.info("Updating previous account ban of longer duration");
               var2 = var15.f();
               var9 = var15.d();
            }
         }

         logger.info("Banning " + var1 + " " + (var6 ? "[IP+Account]" : "[Account]") + " for " + me.a(var3, true) + ": " + var2);
         var14.b(new eF((byte[]) null, var12, var7, var9, var13, var2));
         if (var6) {
            var14.b(new eF(var11, (UUID) null, var7, var9, var13, var2));
         }

         this.a(var1, var2, "banned for " + me.a(var3, true));
      }
   }

   public void a(Jt var1, boolean var2, String var3, long var4, boolean var6, boolean var7) {
      if (var1 == null) {
         logger.warn("Invalid ChatBlock target: null");
      } else {
         long var8 = System.currentTimeMillis();
         long var10 = var8 + var4;
         byte[] var12 = var1.f().b().i();
         UUID var13 = var1.f().D();
         String var14 = var1.f().E();
         qo var15 = this.c.m().R();
         if (var6) {
            eF var16;
            if (var7) {
               var16 = var15.a(var12, var2);
               if (var16 != null && var16.d() > var10) {
                  logger.info("Updating previous IP ChatBlock of longer duration");
                  var3 = var16.f();
                  var10 = var16.d();
               }
            }

            var16 = var15.a(var13, var2);
            if (var16 != null && var16.d() > var10) {
               logger.info("Updating previous account ChatBlock of longer duration");
               var3 = var16.f();
               var10 = var16.d();
            }
         }

         logger.info("ChatBlock(" + (var2 ? "All" : "Team") + ") " + var1 + " " + (var7 ? "[IP+Account]" : "[Account]") + " for " + me.a(var4, true) + ": " + var3);
         var15.b(new eF((byte[]) null, var13, var8, var10, var14, var3), var2);
         if (var7) {
            var15.b(new eF(var12, (UUID) null, var8, var10, var14, var3), var2);
         }

      }
   }

   public void k() {
      List var1 = this.d().m().i();
      this.m = (this.m + 1) % var1.size();
      this.c((String) var1.get(this.m));
   }

   public String l() {
      return (String) this.c(1).get(0);
   }

   public List c(int var1) {
      ArrayList var2 = new ArrayList(var1);
      List var3 = this.d().m().i();
      int var4 = this.m;

      for (int var5 = 0; var5 < var1; ++var5) {
         var4 = (var4 + 1) % var3.size();
         var2.add((String) var3.get(var4));
      }

      return var2;
   }

   public void b(String var1) {
      List var2 = this.d().m().i();
      int var3 = var2.indexOf(var1);
      if (var3 < 0) {
         logger.error("Tried to set next map to " + var1 + " but it\'s not in the list.", new Throwable());
      } else {
         --var3;
         if (var3 < 0) {
            var3 = var2.size() - 1;
         }

         this.m = var3;
         String var4 = (String) var2.get((this.m + 1) % var2.size());
         if (!ty.a((Object) var4, (Object) var1)) {
            logger.info("Set next map result (" + var4 + ") does not match target (" + var1 + ")");
         }

      }
   }

   public void c(String var1) {
      try {
         this.d().a(true);
         logger.info("Changing map: " + var1);

         try {
            da var2 = JL.a("mapLoading");
            var2.a("map", (Object) var1);
            this.c.a(var2);
         } catch (Exception var9) {
            logger.error(var9, var9);
         }

         allowedPerks.clear();

         this.x.clear();
         this.c.i().a();
         this.k = var1;
         String var11 = var1 + "." + "altx";
         if (this.d.E()) {
            this.l = new qm("test.altx", 0, 0L);
         } else {
            this.l = qm.a(fe.f, var11);
         }

         this.n = (this.n + 1) % 4;
         Iterator var4 = this.f().iterator();

         while (var4.hasNext()) {
            Jt var12 = (Jt) var4.next();
            this.a(var12, this.l);
         }

         this.j.clear();
         this.e.a();
         DZ var13 = this.n().ab().f().a();
         this.n().J().a(var13, this.n, this.l);

         try {
            da var14 = JL.a("mapChange");
            var14.a("map", (Object) var1);
            var14.a("mode", (Object) this.n().J().u().g().l().l);
            List var5 = this.n().J().u().g().k().i();
            oD var6 = oD.c;
            oD var7 = oD.c;
            if (var5.size() == 2) {
               var6 = (oD) var5.get(0);
               var7 = (oD) var5.get(1);
            }

            var14.a("leftTeam", var6.c());
            var14.a("rightTeam", var7.c());
            this.c.a(var14);
         } catch (Exception var8) {
            logger.error(var8, var8);
         }
      } catch (Exception var10) {
         String var3 = "Failed to change to map: " + var1;
         if (this.l == null) {
            throw new RuntimeException(var3, var10);
         }

         logger.error(var3, var10);
      }

   }

   private void a(Jt var1, qm var2) {
      var1.a(false);

      if (this.useMinimalConf) {
         var1.a(this.confMinimal);
      } else {
         var1.a(this.o);
      }
      this.g.remove(var1);

      if (this.useMinimalConf) {
         this.c.j().a(var1.e(), this.n, var2, this.confMinimal);
      } else {
         this.c.j().a(var1.e(), this.n, var2, this.o);
      }

      var1.c();
   }

   private oD a(Jt var1) {
      cV var2 = this.n().T();
      List var3 = var2.i();
      int[] var4;
      int var9;
      if (var3.size() == 2) {
         if (var1.m()) {
            return (oD) var3.get(var1.e().h() % 2);
         } else {
            var4 = new int[2];
            double[] var12 = new double[2];
            Iterator var16 = this.h().iterator();

            while (var16.hasNext()) {
               Jt var14 = (Jt) var16.next();
               oD var17 = var14.h().e().c();
               var9 = var3.indexOf(var17);
               if (!var14.m() && var9 >= 0) {
                  ++var4[var9];
                  var12[var9] += var14.r();
               }
            }

            if (var4[0] == var4[1]) {
               double var15 = (double) (var4[0] + var4[1]) / (var12[0] + var12[1]);
               if (Double.isNaN(var15)) {
                  var15 = 0.0D;
               }

               boolean var18 = var1.r() > var15;
               oD var19 = var12[0] <= var12[1] ? (oD) var3.get(0) : (oD) var3.get(1);
               oD var20 = var12[0] <= var12[1] ? (oD) var3.get(1) : (oD) var3.get(0);
               return var18 ? var19 : var20;
            } else if (var4[0] < var4[1]) {
               return (oD) var3.get(0);
            } else {
               return (oD) var3.get(1);
            }
         }
      } else {
         var4 = new int[oD.n.length];

         oD var7;
         for (Iterator var6 = this.h().iterator(); var6.hasNext(); ++var4[var7.c()]) {
            Jt var5 = (Jt) var6.next();
            var7 = var5.n();
         }

         int var11 = Integer.MAX_VALUE;
         int var13 = -1;
         Iterator var8 = var3.iterator();

         while (var8.hasNext()) {
            var7 = (oD) var8.next();
            var9 = var7.c();
            int var10 = var4[var9];
            if (var10 < var11) {
               var11 = var10;
               var13 = var9;
            }
         }

         return oD.a(var13);
      }
   }

   private void b(Jt var1) {
      if (this.g.contains(var1)) {
         logger.error("Duplicate synch for " + var1);
      } else {
         var1.b(this.o);
         Iterator var3 = this.g.iterator();

         while (var3.hasNext()) {
            Jt var2 = (Jt) var3.next();
            Bi var4 = new Bi(this.d, true, var2.g(), var2.h().e());
            var1.a(this.e.a((kY) var4));
         }

         var1.a(this.a(var1));
         this.g.add(var1);
         Or var8 = new Or(new aaf(var1.f()), jV.a, oD.c, mr.b(), 0);
         this.c(var1, new Bi(this.d, true, var1.g(), var8));
         mg var9 = new mg(this.d, true);
         var1.a((kY) var9);
         var1.a((kY) (new h(this.d, this)));
         var1.a((kY) (new PZ(this.d, var1.g(), var1.n())));
         JB[] var10 = this.m().e();

         for (int var5 = 0; var5 < var10.length; ++var5) {
            JB var6 = var10[var5];
            if (var6 != null && !(var6 instanceof Kn) && var6.a()) {
               oa var7 = new oa(this.d, var5, var6.d());
               var1.a(this.e.a((kY) var7));
            }
         }

         var1.a(true);
         this.u();
      }
   }

   private void u() {
      Collection var1 = this.f();
      ArrayList var2 = new ArrayList();
      Iterator var4 = this.g.iterator();

      Jt var3;
      while (var4.hasNext()) {
         var3 = (Jt) var4.next();
         if (!var1.contains(var3)) {
            var2.add(var3);
         }
      }

      if (var2.size() > 0) {
         logger.error("-------client inconsistency: " + var1.size() + " vs " + this.g.size());
         logger.error(var1);
         logger.error("--------vs--------");
         logger.error(this.g);
         logger.error("------------------------");
         var4 = var2.iterator();

         while (var4.hasNext()) {
            var3 = (Jt) var4.next();

            try {
               DZ var5 = var3.e();
               this.a(var3, "failed to distinguish multiple connections from your IP on port " + var5.h(), "connection lost");
               this.f.put(var5, var3);
               this.a(var3, "failed to distinguish multiple connections from your IP on port " + var5.h(), "connection lost");
            } catch (Exception var6) {
               logger.error(var6, var6);
            }
         }
      }

   }

   public void a(DZ var1, qm var2) {
      if (!this.f.containsKey(var1)) {
         logger.info("Unconnected load-complete from non-client IP " + var1);
      } else {
         Jt var3 = (Jt) this.f.get(var1);
         if (!var2.equals(this.l)) {
            logger.info("Out of date load-complete from " + var3);
         } else if (this.g.contains(var3)) {
            logger.info("Duplicate load-complete from " + var3);
         } else {
            this.b(var3);
         }
      }
   }

   private void b(Jt var1, kY var2) {
      var2.a();
   }

   private void c(Jt var1, kY var2) {
      if (!var2.c()) {
         this.d(var1, var2);
      } else {
         this.b(var1, var2);
         Zh var3 = this.e.a(var2);
         Iterator var5 = this.h().iterator();

         while (var5.hasNext()) {
            Jt var4 = (Jt) var5.next();
            var4.a(var3);
         }

      }
   }

   private void d(Jt var1, kY var2) {
   }

   public void a(kY var1) {
      this.a((Jt) null, (kY) var1);
   }

   protected void a(Jt var1, kY var2) {
      var2.b(var1);
      if (!var2.c()) {
         this.d(var1, var2);
      } else {
         this.b(var1, var2);
         if (var2.b()) {
            Zh var3 = this.e.a(var2);
            Iterator var5 = this.h().iterator();

            while (var5.hasNext()) {
               Jt var4 = (Jt) var5.next();
               if (var2.a(var4)) {
                  var4.a(var3);
               }
            }
         }

      }
   }

   private void v() {
      try {
         this.p.clear();
         Iterator var2 = this.f().iterator();

         Jt var1;
         while (var2.hasNext()) {
            var1 = (Jt) var2.next();

            try {
               var1.i();
            } catch (Exception var5) {
               logger.error("Failed to execute post-state modifiers for " + var1, var5);
            }

            if (var1.k()) {
               logger.info("Unresponsive client " + var1 + "; " + var1.l());
               this.p.put(var1, "Connection to game server timed out");
            }
         }

         var2 = this.h().iterator();

         while (var2.hasNext()) {
            var1 = (Jt) var2.next();

            try {
               gk var3 = new gk();
               var3.b(2, this.n);
               var1.a((nu) var3);
               this.a((DZ) var1.e(), (zq) zq.a, (gk) var3);
            } catch (Exception var4) {
               logger.error("Registering broken client: " + var1, var4);
               this.p.put(var1, "Failed to create game update");
            }
         }

         JB[] var7 = this.m().e();

         for (int var8 = 0; var8 < var7.length; ++var8) {
            if (var7[var8] != null) {
               var7[var8].b();
            }
         }

         if (this.p.size() > 0) {
            Iterator var9 = this.p.entrySet().iterator();

            while (var9.hasNext()) {
               Entry var10 = (Entry) var9.next();
               this.a((Jt) var10.getKey(), (String) var10.getValue(), "connection lost");
            }

            this.p.clear();
         }
      } catch (Exception var6) {
         logger.error(var6, var6);
      }

   }

   public Wv m() {
      return this.e;
   }

   public fe n() {
      return this.d;
   }

   public int a(nq var1) {
      int var2 = this.m().g();
      this.j.put(var1, Integer.valueOf(var2));
      return var2;
   }

   public int a(Jt var1, nq var2, int var3) {
      int var4 = this.a(var2);
      var1.a(var3, var4);
      var1.a((kY) (new kd(this.n(), var3, var4)));
      return var4;
   }

   public int b(nq var1) {
      if (this.j.containsKey(var1)) {
         return ((Integer) this.j.remove(var1)).intValue();
      } else {
         throw new IllegalStateException();
      }
   }

   public boolean a(lY var1) {
      int var2 = this.g();
      boolean var3 = var2 < this.c.m().d();
      boolean var4 = false;
      if (this.d().m().l().contains(var1.D())) {
         var4 = true;
      } else if (this.r() && this.s().containsKey(var1.D())) {
         var4 = true;
      } else if (var1.b().k()) {
         var4 = true;
      }

      return var2 < 200 && (var3 || var4);
   }

   private void w() {
      if (this.v > 0) {
         --this.v;
         if (this.v == 0) {
            this.o();
         }
      }

      Iterator var3;
      if (this.r() && this.u-- <= 0) {
         this.u = 30;
         int players = 0;
         var3 = this.z.keySet().iterator();

         while (var3.hasNext()) {
            UUID var2 = (UUID) var3.next();
            if (this.b(var2) != null) {
               ++players;
            }
         }

         if (players == 0) {
            boolean auto_stop = Config.get(port()).auto_stop_tournament;
            if (auto_stop) {
               logger.info("All tournament players left.  Automatically calling stopTournament.");
               this.q();
            }
         }

      }

      if (this.t > 0) {
         --this.t;
         if (this.t == 0) {
            this.x();
            this.t = s;
         }
      }

      if (this.pingSummaryCounter > 0) {
         --this.pingSummaryCounter;
         if (this.pingSummaryCounter == 0) {
            if (this.g() > 0) {
               try {
                  HashMap var5 = new HashMap();
                  var3 = this.f().iterator();

                  while (var3.hasNext()) {
                     Jt var6 = (Jt) var3.next();
                     if (!var6.m()) {
                        var5.put(Integer.valueOf(var6.g()), Integer.valueOf(var6.h().j()));
                     }
                  }

                  da var7 = JL.a("pingSummary");
                  var7.a("pingByPlayer", (Map) var5);
                  this.c.a(var7);
               } catch (Exception var4) {
                  logger.error(var4, var4);
               }
            }
         }
      } else {
         if(Config.get(port()).ping_log_time != 0) {
            pingSummaryCounter = Math.round(sx.d(Config.get(port()).ping_log_time).a());
         }
      }

      if (positionLogCounter > 0.0) {
         //--positionLogCounter;
         positionLogCounter = positionLogCounter - positionLogCounter;
         if (positionLogCounter == 0.0) {
            positionLogCounter = Config.get(port()).position_log_time;
            if (this.g() > 0) {
               try {
                  HashMap<Integer, String> map = new HashMap<>();
                  for (Object o : this.f()) {
                     Jt client = (Jt) o;
                     mN plane = client.h().i();

                     int x = -1, y = -1, angle = -1;
                     if (plane != null && plane.aO()) {
                        x = Math.round(plane.f_().c);
                        y = Math.round(plane.f_().d);
                        angle = Math.round(plane.m());
                     }

                     String out = x + "," + y;
                     if(Config.global.features.log_plane_angle){
                        out += "," + angle;
                     }

                     map.put(client.g(), out);

                  }
                  da log = JL.a("logPlanePositions");
                  log.a("positionByPlayer", (Map) map);
                  this.c.a(log);
               } catch (Exception e) {
                  logger.error(e, e);
               }
            }
         }
      } else {
         if(Config.get(port()).position_log_time != 0) {
            positionLogCounter = Config.get(port()).position_log_time;
         }
      }

   }

   private double b(mF var1) {
      Jt var2 = this.a(var1.h());
      return var2 != null ? var2.r() : 0.0D;
   }

   public void o() {
      this.v = 0;
      if (this.n().T().i().size() == 2) {
         ArrayList var1 = new ArrayList(this.d.J().u().j());

         for (int var2 = var1.size() - 1; var2 >= 0; --var2) {
            if (ty.a((Object) oD.c, (Object) ((mF) var1.get(var2)).e().c())) {
               var1.remove(var2);
            }
         }

         Collections.sort(var1, this.w);
         ArrayList var11 = new ArrayList();
         List var3 = this.d.T().i();

         int var4;
         for (var4 = 0; var4 < var3.size(); ++var4) {
            var11.add(new ArrayList());
         }

         var4 = 0;

         int var5;
         for (var5 = 0; var5 < var1.size(); ++var5) {
            mF var6 = (mF) var1.get(var5);
            int var7 = (var5 + 1) / 2 % 2 == 0 ? 0 : 1;
            if (!var6.e().c().equals(var3.get(var7))) {
               ++var4;
            }

            ((List) var11.get(var7)).add(var6);
         }

         if (var4 > var1.size() / 2) {
            Collections.reverse(var11);
            var4 = var1.size() - var4;
         }

         for (var5 = 0; var5 < var11.size(); ++var5) {
            List var12 = (List) var11.get(var5);
            oD var13 = (oD) var3.get(var5);

            mF var8;
            mN var10;
            for (Iterator var9 = var12.iterator(); var9.hasNext(); this.a((kY) (new fv(this.d, var8.h(), var13, var10)))) {
               var8 = (mF) var9.next();
               var10 = var8.i();
               if (var10 == null || !var10.aO()) {
                  var10 = null;
               }
            }
         }

         this.a("Balance Teams Complete");
      }
   }

   private void x() {
      cV var1 = this.n().T();
      if (var1 != null && var1.g()) {
         int var2 = 0;
         Jt var3 = null;
         oD var4 = null;
         List var5 = this.h();

         for (int var6 = 0; var6 < var5.size(); ++var6) {
            Jt var7 = (Jt) var5.get(var6);
            if (var7.m()) {
               oD var8 = var7.h().e().c();
               if (!ty.a((Object) var8, (Object) oD.c)) {
                  oD var9 = xX.a(this.n(), var7);
                  if (!var9.equals(var8)) {
                     int var10 = 1000;
                     mN var11 = var7.h().i();
                     if (var11 != null && var11.aO()) {
                        if (!var11.av() && !var11.aw() && var11.aR().w() >= 30) {
                           var10 -= 500;
                        } else {
                           var10 = 0;
                        }
                     }

                     if (var10 > var2) {
                        var2 = var10;
                        var3 = var7;
                        var4 = var9;
                     }
                  }
               }
            }
         }

         if (var3 != null) {
            mN var12 = var3.h().i();
            if (var12 != null && var12.aO()) {
               this.a((kY) (new IU(this.n(), var12, var12, var12.ak(), 0.0F)));
            }

            this.a((kY) (new fv(this.d, var3.g(), var4, var12)));
         }

      }
   }

   public void a(mF var1, int var2, int var3, int var4) {
      int var5 = var1.h();
      if (var2 == 0 && var3 == 0 && var4 == 0) {
         this.x.remove(Integer.valueOf(var5));
      } else {
         Dl var6 = new Dl();
         var6.a = (float) var2;
         var6.b = (float) var3;
         var6.c = (float) var4;
         this.x.put(Integer.valueOf(var5), var6);
      }

   }

   public Dl a(mF var1) {
      int var2 = var1.h();
      return (Dl) this.x.get(Integer.valueOf(var2));
   }

   public int port() {
      return this.c.m().a();
   }

   public void p() {
      ArrayList allPlayers = new ArrayList(this.d.J().u().j());
      ArrayList players = new ArrayList();

      for (Object aVar1 : allPlayers) {
         mF player = (mF) aVar1;
         // If the player is not a spectator, and either not a bot or bots are allowed..
         if (!player.e().c().equals(oD.c) && (!player.a() || Config.get(port()).bots.allow_join_tournament))
            players.add(player);
      }

      Collections.sort(players, new zf(this));
      StringBuilder msg = new StringBuilder();
      msg.append("\n------===={ Starting Tournament:" + (new Date(System.currentTimeMillis())).toString() + " }====------\n");
      CE.a("", 2,  msg);
      CE.a("[Team]", 7,  msg);
      CE.a("[Name]", 20,  msg);
      CE.a("[VaporId]", 40,  msg);
      CE.a("[IP]", 32,  msg);
      msg.append("\n");
      this.z = new HashMap();
      Iterator var5 = players.iterator();

      while (var5.hasNext()) {
         mF var12 = (mF) var5.next();
         aaf var6 = var12.e().a();
         UUID var7 = var6.D();
         CE.a("", 6, (StringBuilder) msg);
         int var8 = this.d.T().i().indexOf(var12.e().c());
         CE.a(String.valueOf(var8), 3, (StringBuilder) msg);
         CE.a(var6.E(), 20, (StringBuilder) msg);
         CE.a(var7.toString(), 40, (StringBuilder) msg);
         String var9 = "";
         Jt var10 = this.a(var12.h());
         if (var10 != null) {
            var9 = var10.e().toString();
         }

         CE.a(var9, 32, (StringBuilder) msg);
         msg.append("\n");
         this.z.put(var7, Integer.valueOf(var8));
      }

      msg.append("------===============================------\n\n");
      logger.info(msg.toString());
      this.a("Tournament started.  Only players currently on a team will be allowed to spawn.");
      this.y = true;
      this.b();
      this.d("tournamentStart");
   }

   private void d(String var1) {
      ArrayList var2 = new ArrayList();
      ArrayList var3 = new ArrayList();
      Iterator var5 = this.z.keySet().iterator();

      while (var5.hasNext()) {
         UUID var4 = (UUID) var5.next();
         boolean var6 = ((Integer) this.z.get(var4)).intValue() == 0;
         ArrayList var7 = var6 ? var2 : var3;
         var7.add(var4.toString());
      }

      try {
         da var9 = JL.a(var1);
         var9.a("team0", (Collection) var2);
         var9.a("team1", (Collection) var3);
         this.c.a(var9);
      } catch (Exception var8) {
         logger.error(var8, var8);
      }

   }

   public void a(mF var1, int var2) {
      logger.info("\n------===={ Modifying Tournament: playerId=" + var1.h() + " to team=" + var2 + " }====------\n");
      UUID var3 = var1.e().a().D();
      this.z.remove(var3);
      if (var2 != -1) {
         this.z.put(var3, Integer.valueOf(var2));
      }

      this.b();
      this.d("tournamentModified");
   }

   public void q() {
      this.y = false;
      logger.info("\n------===={ Stopping Tournament }====------\n");
      this.a("Tournament stopped.  All players can now spawn.");
      this.b();

      try {
         da var1 = JL.a("tournamentStop");
         this.c.a(var1);
      } catch (Exception var2) {
         logger.error(var2, var2);
      }

   }

   public boolean isTournament() {
      return this.y;
   }
   public boolean r() { return this.isTournament(); }

   public Map s() {
      return this.z;
   }

   public oD c(UUID var1) {
      Integer var2 = (Integer) this.z.get(var1);
      return var2 != null && var2 <= this.d.T().i().size() ?
              (oD) this.d.T().i().get(var2) :
              oD.c;
   }

   public int a(tK var1) {
      List var2 = this.d.J().u().j();

      int var3;
      for (var3 = 0; var3 < var2.size(); ++var3) {
         mF var4 = (mF) var2.get(var3);
         boolean var5 = var4.i() != null && var4.i().aO();
         jV var6 = var5 ? var4.i().R() : var4.e().b();
         if (var6.n() && !ty.a((Object) oD.c, (Object) var4.e().c())) {
            return 0;
         }
      }

      if (this.d.Y() - this.A < 300) {
         return 0;
      } else {
         var3 = ((ArrayList) RR.g.get(var1)).size();
         if (var3 > 1 && dh.a(3)) {
            this.A = this.d.Y();
            return dh.a(1, var3);
         } else {
            return 0;
         }
      }
   }

   public int t() {
      return this.m;
   }

   public boolean a(UC var1) {
      return this.c.m().A().contains(var1);
   }

   // $FF: synthetic method
   static double a(VS var0, mF var1) {
      return var0.b(var1);
   }

   // $FF: synthetic method
   static fe a(VS var0) {
      return var0.d;
   }
}
