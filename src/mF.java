import java.util.UUID;

import mods.Config;
import org.apache.log4j.Logger;

public class mF {
   private static final Logger d = Logger.getLogger(mF.class);
   public static final UUID a = new UUID(0L, 0L);
   public static final UUID b = new UUID(777L, 777L);
   private static final Or e;
   public static final mF c;
   private fe f;
   private yV g;
   private int h;
   private Or i;
   private String j;
   private String k;
   private String l;
   private mN m;
   private xs n;
   private int o;
   private mN p;

   static {
      e = new Or(new aaf(a, "<PLAYER>"));
      c = new mF((fe)null, (yV)null, -2, e);
   }

   public mF(fe var1, yV var2, int var3, Or var4) {
      this.f = var1;
      this.g = var2;
      this.h = var3;
      this.a(var4);
      this.f();
   }

   public boolean a() {
      /*if(f == null) {
         return false;
      }
      int port = f.u().c().port();
      Config.get(port).bots.name_prefix
      */
      // A referemce to server launcher is in this.f but this is null most of the time
      // Disabled the bot name check
      // TODO: In which case players could have a UUID of 0 and it would interfere with bots behaviour,
      // <PLAYER>, is shown when a client disconnects just in the right moment, i guess if you access during that time
      // this function it also returns true for non-bots
      return this.i.a().D().equals(a) /*&& this.i.a().E().startsWith("Mr. ")*/;
   }

   public String toString() {
      return Integer.toString(this.h);
   }

   public void a(Or var1) {
      if (this.f != null && this.f.B() && var1 != null) {
         oD var2 = this.i == null ? oD.b : this.i.c();
         if (!ty.a((Object)var2, (Object)var1.c())) {
            try {
               da var3 = JL.a("teamChange");
               var3.a("player", this.h);
               var3.a("team", var1.c().c());
               this.f.u().c().a(var3);
            } catch (Exception var4) {
               d.error(var4, var4);
            }
         }
      }

      this.i = var1;
      this.b();
   }

   public void b() {
      this.j = this.l();
      this.k = this.j;
      this.l = this.j;
      if (this.g != null && this.g.j() != null && this.g.j().i() != null) {
         AK var1 = this.g.j().i();
         boolean var2 = var1.y();
         boolean var3 = ty.a((Object)var1.D(), (Object)this.i.a().D());
         if (var2 && !var3 && !this.a()) {
            this.k = "Player " + this.h;
            this.l = "Player " + this.h;
         }
      }

      if (RU.M() != null) {
         this.k = RU.M().b(this.k);
         this.l = RU.M().a(this.l);
      } else {
         this.l = XY.a(this.l);
      }

   }

   private void k() {
      if (!ty.a((Object)this.j, (Object)this.l())) {
         this.b();
      }

   }

   private String l() {
      return this.i.a().E();
   }

   public String c() {
      this.k();
      return this.k;
   }

   public String d() {
      this.k();
      return this.l;
   }

   public Or e() {
      return this.i;
   }

   public void f() {
      this.n = new xs();
   }

   public xs g() {
      return this.n;
   }

   public int h() {
      return this.h;
   }

   public void a(mN var1) {
      this.p = var1;
   }

   public void b(mN var1) {
      if (this.m != var1) {
         if (this.m != null) {
            this.m.aJ();
         }

         this.m = var1;
         if (this.p != null) {
            this.p.a(this.m);
            this.p = null;
         }
      }

   }

   public mN i() {
      return this.m;
   }

   public void a(int var1) {
      this.o = dh.a(var1, 0, 999);
   }

   public int j() {
      return this.o;
   }
}
