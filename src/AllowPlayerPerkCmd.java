import java.util.HashSet;
import java.util.UUID;

class AllowPlayerPerkCmd extends Command {
   private IntParam player_no;
   private StringParam perk_name;

   public AllowPlayerPerkCmd(fe game) {
      super(game, "allowPlayerPerk", PermissionGroup.admin);
      this.perk_name = new StringParam("perkName", "The name of the perk to allow");
      this.player_no = new IntParam("playerNo", "Player number");
      this.setParams(new JI[]{perk_name, player_no});
   }

   protected void execute(int[] a, String... b) {
      dp playerCont = this.getGame().J().u();
      mF player = playerCont.c(player_no.value);
      if(player != mF.c) {
         UUID vaporID = player.e().a().D();
         if (!this.getServer().l().allowedPerks.containsKey(vaporID)) {
            this.getServer().l().allowedPerks.put(vaporID, new HashSet<String>());
         }
         this.getServer().l().allowedPerks.get(vaporID).add(this.perk_name.value);
      } else {

      }
   }
}
