import org.hjson.JsonArray;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

/** An abstract server command, which wraps altitudes own with deobfuscated methods.
 * To add a new command, extend this class and register it in `ServerPatches`. */
public abstract class Command extends Os {

   /** By whom a command can use a command when using the default/vanilla
    *  permission system */
   enum PermissionGroup {
      admin, anon, vote
   }

   public Command(fe game, String name, PermissionGroup permission) {
      super(name, game);
      this.a(true);
      switch (permission) {
         case admin:
            this.b(YY.b);
            break;
         case anon:
            this.b(YY.c);
            break;
         case vote:
            this.b(YY.d);
            break;
      }
   }

   /** Set the command's parameters -- usually called in the constructor */
   void setParams(JI[] params) {
      this.a(params);
   }

   /** Get the command's json definition, as in `custom_json_commands.txt` */
   String toJson() {
      JsonObject o = new JsonObject();
      o.add("name", this.f());
      o.add("voteThreshold", (int) this.c() * 100);
      JsonArray args = new JsonArray();
      JsonArray arg_desc = new JsonArray();
      for (JI param : this.b(new String[]{""})) {
         if (param instanceof FloatParam || param instanceof StringParam) {
            args.add("string");
            arg_desc.add(param.getDescription());
         }
         if (param instanceof IntParam) {
            args.add("int");
            arg_desc.add(param.getDescription());
         }
         if (param instanceof GO) {
            JsonArray a = new JsonArray();
            a.add("true");
            a.add("false");
            args.add(a);
            arg_desc.add(param.getDescription());
         }
      }
      o.add("arguments", args);
      if (arg_desc.size() > 0) {
         o.add("argumentDescriptions", arg_desc);
      }
      return o.toString();
   }

   /** Send a log event to the JSON log */
   protected void log(da log) {
      this.getGame().u().c().a(log);
   }

   protected fe getGame() {
      return this.j;
   }

   protected fN getServer() {
      return this.getGame().u().c();
   }

   protected boolean a(NB var1, FW var2, Jt var3, String[] var4) {
      return true;
   }

   protected abstract void execute(int[] a, String... b);

   protected void a(int[] a, String... b) {
      this.execute(a, b);
   }

   public String a() {
      return null;
   }

   public String b() {
      return null;
   }
}
