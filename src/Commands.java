import mods.Config;
import org.apache.log4j.Logger;
import org.hjson.JsonArray;
import org.hjson.JsonValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Commands {
   private static final Logger logger = Logger.getLogger(ServerPatches.class);
   private static List<Class> commands = new ArrayList<>();

   static void register_command(Class command) {
      Commands.commands.add(command);
   }

   static List<Command> get_commands(fe game) {
      List<Command> list = new ArrayList<>();
      for (Class c : Commands.commands) {
         try {
            list.add((Command) c.getConstructor(new Class[]{fe.class}).newInstance(game));
         } catch (Exception e) {
            logger.error("Failed to construct command", e);
         }
      }
      return list;
   }

   static List<String> get_all_commands() {
      List<Command> list = get_commands(null);
      List<String> json = new ArrayList<>();
      for (Command x : list) {
         json.add(x.toJson());
      }
      return json;
   }

   static List<String> get_client_commands() {
      List<Command> list = get_commands(null);
      List<String> json = new ArrayList<>();
      List<String> client_commands = Config.global.client_commands;
      for (Command x : list) {
         if (client_commands.contains(x.f())) json.add(x.toJson());
      }
      return json;
   }

}
