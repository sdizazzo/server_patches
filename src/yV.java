import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import mods.Config;
import org.apache.log4j.Logger;
import steam.ISteamFriends;
import steam.ISteamUser;
import steam.steam_api;

public class yV implements MI, iX {
   protected static final Logger a = Logger.getLogger(yV.class);
   public static final int c = 31373;
   public static final int d = 31383;
   public static boolean e = false;
   private xK l;
   private lm m;
   private WL n;
   private aao o;
   private EG p;
   private tS q;
   private zE r;
   private vf s;
   private Af t;
   private aaz u;
   private final DZ v;
   private final DZ w;
   private Gr x;
   private SocketFactory y;
   private Socket z;
   private Tt A;
   tB f;
   DC g;
   private final List B;
   private static final char[] C = "Va1por cli@ent co~nf!i9g pa`ss^phr;ase. 1)*$!3680.".toCharArray();
   private static final char[] D = "Va1por cli@ent tr)ust keys&tore pass^phrase. 149#1-0aE<X.".toCharArray();
   private KeyStore E;
   mY h;
   Hi i;
   private r F;
   private lI G;
   private E H;
   private Ap I;
   private Se J;
   private Gb K;
   private co L;
   private int M;
   public static final Long j = Long.valueOf(1L);
   public static final Long k = Long.valueOf(2L);

   public static DZ a() {
      mods.Config.load();
      return new DZ(Config.global.vapor_address, d);
   }

   public static String getVaporKeystore() {
      return Config.global.vapor_keystore;
   }

   public final boolean a(DZ var1) {
      return a(this.t(), var1);
   }

   public static boolean a(DZ var0, DZ var1) {
      byte[] var2 = var0.i();
      byte[] var3 = var1.i();
      boolean var4 = Arrays.equals(var2, var3);
      if(!var4) {
         a.error("ips not equal", new Throwable());
         a.error("vaporIPBytes is " + Arrays.toString(var2));
         a.error("inIPBytes    is " + Arrays.toString(var3));
      }

      return var4;
   }

   public yV(aaz var1) throws ZM {
      this(var1, a());
   }

   public yV(aaz var1, DZ var2) throws ZM {
      this(var1, var2, 40);
   }

   public yV(aaz var1, DZ var2, int var3) throws ZM {
      this.B = new ArrayList();
      this.F = new r();
      this.M = var3;
      if(!var2.c()) {
         throw new ZM("Failed to resolve vapor server IP.");
      } else {
         this.w = var2;
         this.v = new DZ(var2.g(), var2.i(), c);
         this.K = new Gb(this);
         this.z();
         this.B();
         this.u = var1;
         this.y();
      }
   }

   private void y() throws ZM {
      this.l = new xK(this);
      this.m = new lm(this);
      this.m.a((iX)this);
      this.o = new aao(this);
      this.p = new EG();
      this.q = new tS(this, this.o, this.p, this.M);
      this.r = new zE(this.t());
      this.s = new vf(this);

      try {
         this.n = new WL(this.t(), this.E);
         this.n.a((zp)(new TV(this)));
      } catch (Exception var2) {
         throw new ZM("Unable to create user validator", var2);
      }

      if(this.u != null) {
         this.u.a((byte)Yi.a.ordinal(), (wr)this.l);
         this.u.a((byte)Yi.b.ordinal(), (wr)this.m);
         this.u.a((byte)Yi.c.ordinal(), (wr)this.n);
         this.u.a((byte)Yi.h.ordinal(), this.o.a());
         this.u.a((byte)Yi.i.ordinal(), (wr)this.p);
         this.u.a((byte)Yi.d.ordinal(), (wr)this.q);
         this.u.a((byte)Yi.g.ordinal(), (wr)this.r);
         this.u.a((byte)Yi.j.ordinal(), (wr)this.s);
      }

   }

   public void a(float var1) {
      List var2 = this.F.a();

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         Exception var4 = (Exception)var2.get(var3);
         a.error(var4.getMessage(), var4);
      }

      this.q.a(var1);
      this.m.a(var1);
      this.l.a(var1);
      this.p.a();
   }

   private void z() {
      try {
         File var1 = new File("config/VaporClientConfig.xml");
         e = var1.exists();
         this.x = (Gr)eo.a(var1, Gr.a);
         Ln var2 = new Ln(this);
         var2.a(C, this.x);
      } catch (Exception var3) {
         a.info("Creating default vapor client configuration file.");
         e = false;
         this.x = Gr.c();
      }

   }

   public void b() {
      try {
         Gr var1 = new Gr(this.x);
         Ln var2 = new Ln(this);
         var2.b(C, var1);
         File var3 = new File("config/VaporClientConfig.xml");
         eo.a((File)var3, (Object)var1, (Kl)Gr.a);
      } catch (Exception var4) {
         a.error(var4, var4);
      }

   }

   public void c() {
      this.C();
      if(this.l != null) {
         this.l.c();
      }

      this.l = null;
      if(this.m != null) {
         this.m.c();
      }

      this.m = null;
      if(this.n != null) {
         this.n.c();
      }

      this.n = null;
      if(this.o != null) {
         this.o.a().c();
      }

      this.o = null;
      if(this.p != null) {
         this.p.c();
      }

      this.p = null;
      if(this.q != null) {
         this.q.c();
      }

      this.q = null;
      if(this.r != null) {
         this.r.c();
      }

      this.r = null;
      if(this.s != null) {
         this.s.c();
      }

      this.s = null;
      this.z = null;
      this.A = null;
      this.f = null;
      this.g = null;
   }

   public void a(DC var1) {
      this.g = var1;
   }

   public void a(cO var1, CG var2) {
      cO var3 = var1.a();
      TU var4 = new TU(this, this.F, var3, var2);
      var4.setDaemon(true);
      this.a((nM)var4);
      var4.start();
   }

   public synchronized Tt a(cO var1) throws IOException {
      try {
         this.D();
         this.t.a(var1);
         this.t.a();
      } finally {
         this.C();
      }

      return this.A;
   }

   public void a(IA var1, iN var2) {
      IA var3 = var1.a();
      TT var4 = new TT(this, this.F, var3, var2);
      var4.setDaemon(true);
      this.a((nM)var4);
      var4.start();
   }

   public synchronized lI a(IA var1) throws IOException {
      try {
         this.D();
         this.t.a(var1);
         this.t.a();
      } finally {
         this.C();
      }

      return this.G;
   }

   public tB a(Zm var1, TJ var2) throws IOException {
      if(this.u == null || !this.u.a() && !this.u.b()) {
         tB var4 = this.a((Zm)var1, (sD)var2);
         return var4;
      } else {
         TS var3 = new TS(this, this.F, var1, var2);
         var3.setDaemon(true);
         this.a((nM)var3);
         var3.start();
         return null;
      }
   }

   private void a(nM var1) {
      this.A();
      List var2 = this.B;
      synchronized(this.B) {
         this.B.add(var1);
      }
   }

   private void A() {
      List var1 = this.B;
      synchronized(this.B) {
         for(int var2 = 0; var2 < this.B.size(); ++var2) {
            nM var3 = (nM)this.B.get(var2);
            if(!var3.isAlive()) {
               this.B.remove(var2);
               --var2;
            }
         }

      }
   }

   public void d() {
      this.F.b();
      this.A();
      List var1 = this.B;
      synchronized(this.B) {
         for(int var2 = 0; var2 < this.B.size(); ++var2) {
            ((nM)this.B.get(var2)).c();
            ((nM)this.B.get(var2)).stop();
         }

      }
   }

   public synchronized tB a(Zm var1, sD var2) throws IOException {
      try {
         this.i = null;
         this.f = null;
         this.g = null;
         var2.b("SSL handshake");
         a.trace("SSL handshake");
         this.D();
         var2.b("Sending login credentials");
         a.trace("Sending login credentials");
         this.t.a(var1);
         var2.b("Waiting for response");
         a.trace("Waiting for credential response");
         this.t.a();
      } finally {
         this.C();
      }

      if(this.f != null && this.f.h()) {
         if(steam_api.isSteamRunning() && this.f.i().z() == null) {
            try {
               ha var3 = e();
               this.l.a(var3);
            } catch (Throwable var8) {
               a.error(var8, var8);
            }
         }

         var2.b("UDP handshake 1");
         a.trace("UDP handshake 1");
         tB var10 = this.m.a();
         if(!var10.h()) {
            this.f.e(false);
            this.f.a(var10.g());
            this.f.d(false);
            this.f.f(var10.j());
         } else {
            this.f.d(true);
            this.f.a(var10.i());
            this.K.e();
            this.l.a(new pL(this.f.i()));
            this.r.a();
            var2.b("UDP handshake 2");
            a.trace("UDP handshake 2");
            this.g = this.n.b(new MU(this.f.i(), 1));
            if(this.g == null) {
               throw new IOException("Failed UDP handshake 2");
            }

            this.m.b();
            if(this.f.h()) {
               Xm var4 = new Xm();
               var4.a(1);
               var4.a(this.f.i().D());
               var2.b("UDP handshake 3");
               a.trace("UDP handshake 3");
               a.trace("UDP handshake 3.1");
               boolean var5 = this.s.a(this.f.i(), var4);
               var2.b("UDP handshake 4");
               a.trace("UDP handshake 4");
               var2.b("Loading game...");
               a.trace("Loading game...");
               if(!var5) {
                  this.f.e(false);
                  this.f.a("UDP handshake failed");
                  a.trace("UDP handshake failed");
               }
            }
         }
      }

      return this.f;
   }

   public static ha e() {
      Ir var0 = new Ir(Long.valueOf(ISteamUser.a().a()), ISteamFriends.a());
      int var1 = ISteamFriends.a(new steam.b[]{steam.b.d});
      if(var1 < 0 || var1 > 256) {
         var1 = 0;
      }

      ArrayList var2 = new ArrayList(var1);

      for(int var3 = 0; var3 < var1; ++var3) {
         steam.c var4 = ISteamFriends.a(var3, new steam.b[]{steam.b.d});
         Ir var5 = new Ir(Long.valueOf(var4.a()), ISteamFriends.a(var4));
         var2.add(var5);
      }

      ha var6 = new ha(var0, var2);
      return var6;
   }

   public mY a(jn var1, er var2) throws IOException {
      if(this.u == null || !this.u.a() && !this.u.b()) {
         return this.a(var1);
      } else {
         TR var3 = new TR(this, this.F, var1, var2);
         var3.setDaemon(true);
         this.a((nM)var3);
         var3.start();
         return null;
      }
   }

   public synchronized mY a(jn var1) throws IOException {
      try {
         this.i = null;
         this.h = null;
         this.D();

         try {
            this.t.a(var1);
         } catch (Exception var6) {
            a.error("On write Exception");
            a.error(var6, var6);
         }

         this.t.a();
      } finally {
         this.C();
      }

      return this.h;
   }

   public Se a(jc var1, al var2) throws IOException {
      if(this.u == null || !this.u.a() && !this.u.b()) {
         return this.a(var1);
      } else {
         TP var3 = new TP(this, this.F, var1, var2);
         var3.setDaemon(true);
         this.a((nM)var3);
         var3.start();
         return null;
      }
   }

   public synchronized Se a(jc var1) throws IOException {
      try {
         this.i = null;
         this.J = null;
         this.D();

         try {
            this.t.a(var1);
         } catch (Exception var6) {
            a.error(var6, var6);
         }

         this.t.a();
      } finally {
         this.C();
      }

      return this.J;
   }

   public Ap a(WE var1, YR var2) throws IOException {
      if(this.u == null || !this.u.a() && !this.u.b()) {
         return this.a(var1);
      } else {
         TN var3 = new TN(this, this.F, var1, var2);
         var3.setDaemon(true);
         this.a((nM)var3);
         var3.start();
         return null;
      }
   }

   public synchronized Ap a(WE var1) throws IOException {
      try {
         this.i = null;
         this.I = null;
         this.D();

         try {
            this.t.a(var1);
         } catch (Exception var6) {
            a.error(var6, var6);
         }

         this.t.a();
      } finally {
         this.C();
      }

      return this.I;
   }

   public co a(nl var1, NI var2) throws IOException {
      if(this.u == null || !this.u.a() && !this.u.b()) {
         co var4 = this.a(var1);
         if(var4 != null) {
            var2.a(var4);
         } else if(this.i != null) {
            var2.a(this.i);
         }

         return var4;
      } else {
         TM var3 = new TM(this, this.F, var1, var2);
         var3.setDaemon(true);
         this.a((nM)var3);
         var3.start();
         return null;
      }
   }

   public synchronized co a(nl var1) throws IOException {
      try {
         this.i = null;
         this.I = null;
         this.D();

         try {
            this.t.a(var1);
         } catch (Exception var6) {
            a.error(var6, var6);
         }

         this.t.a();
      } finally {
         this.C();
      }

      return this.L;
   }

   public void a(Hy var1, Yg var2) {
      TL var3 = new TL(this, this.F, var1, var2);
      var3.setDaemon(true);
      this.a((nM)var3);
      var3.start();
   }

   public synchronized E a(Hy var1) throws IOException {
      try {
         this.i = null;
         this.H = null;
         this.D();

         try {
            this.t.a(var1);
         } catch (Exception var6) {
            a.error(var6, var6);
         }

         this.t.a();
      } finally {
         this.C();
      }

      return this.H;
   }

   public void f() {
      if(this.f != null && this.f.h()) {
         MK var1 = new MK();
         var1.a(this.f.i().f());
         this.m.a(var1);
      }

   }

   private void a(SSLContext var1, TrustManager[] var2) throws KeyManagementException {
      String var3 = System.getProperty("java.io.tmpdir");

      try {
         try {
            System.setProperty("java.io.tmpdir", System.getProperty("user.dir"));
         } catch (Exception var12) {
            a.error(var12, var12);
         }

         var1.init((KeyManager[])null, var2, (SecureRandom)null);
      } finally {
         try {
            System.setProperty("java.io.tmpdir", var3);
         } catch (Exception var11) {
            a.error(var11, var11);
         }

      }

   }

   private void B() throws ZM {
      try {
         SSLContext var1 = SSLContext.getInstance("TLS");
         this.E = g();
         TrustManagerFactory var2 = TrustManagerFactory.getInstance("SunX509");
         var2.init(this.E);
         this.a(var1, var2.getTrustManagers());
         this.y = var1.getSocketFactory();
      } catch (Exception var3) {
         throw new ZM("Unable to create socket factory. " + var3.getMessage(), var3);
      }
   }

   public static KeyStore g() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
      KeyStore var0 = KeyStore.getInstance("JCEKS");
      File var1 = new File("vapor/VaporClientTrust.jks");
      Object var2;
      if (var1.exists()) {
         var2 = new ByteArrayInputStream(gj.a(var1));
      } else {
         var2 = yV.class.getClassLoader().getResourceAsStream(getVaporKeystore());
         if (var2 == null) {
            throw new IOException("Cannot find keystore: vapor/VaporClientTrust.jks");
         }
      }

      try {
         var0.load((InputStream) var2, D);
      } finally {
         ((InputStream) var2).close();
      }

      return var0;
   }

   private void C() {
      this.t = null;
      if(this.z != null) {
         try {
            this.z.close();
         } catch (IOException var2) {
            a.error(var2, var2);
         }
      }

      this.z = null;
   }

   private void D() throws IOException {
      InetAddress var1 = this.v.g();
      int var2 = this.v.h();
      InetAddress var3 = DZ.a();
      int var4 = this.x.b();
      this.z = this.y.createSocket(var1, var2, var3, var4);
      this.z.setSoTimeout(lm.a);
      this.t = new Af(this, this.z.getInputStream(), this.z.getOutputStream());
   }

   public void a(Tt var1) {
      this.A = var1;
   }

   public void a(lI var1) {
      this.G = var1;
      if(this.G.b()) {
         this.f.a(this.G.i());
         this.K.e();
      }

   }

   public void a(tB var1) {
      this.f = var1;
   }

   public void a(mY var1) {
      this.h = var1;
   }

   public void a(Hi var1) {
      this.i = var1;
   }

   public void a(Ap var1) {
      this.I = var1;
   }

   public void a(E var1) {
      this.H = var1;
   }

   public void a(Se var1) {
      this.J = var1;
   }

   public lm h() {
      return this.m;
   }

   public xK i() {
      return this.l;
   }

   public tB j() {
      return this.f;
   }

   public Gr k() {
      return this.x;
   }

   public WL l() {
      return this.n;
   }

   public String a(int var1) {
      if(this.f != null) {
         Iterator var3 = this.f.e().iterator();

         while(var3.hasNext()) {
            or var2 = (or)var3.next();
            if(var2.a() == var1) {
               return var2.b();
            }
         }
      }

      return null;
   }

   public tS m() {
      return this.q;
   }

   public zE n() {
      return this.r;
   }

   public void a(Nc var1) {
      this.f = null;
      this.g = null;
      this.c();

      try {
         this.y();
      } catch (ZM var3) {
         a.error(var3, var3);
      }

   }

   public aaz o() {
      return this.u;
   }

   public PublicKey p() {
      PublicKey var1 = null;

      try {
         var1 = this.E.getCertificate("VaporServerPublicKey").getPublicKey();
      } catch (KeyStoreException var3) {
         a.error(var3, var3);
      }

      return var1;
   }

   public static String q() {
      return "RSA";
   }

   public vf r() {
      return this.s;
   }

   public void a(co var1) {
      this.L = var1;
   }

   public DZ s() {
      return this.v;
   }

   public DZ t() {
      return this.w;
   }

   public DC u() {
      return this.g;
   }

   public boolean v() {
      return this.f != null && this.f.h() && this.f.i() != null && this.f.i().q() != null;
   }

   public Gb w() {
      return this.K;
   }

   public void b(Nc var1) {
      a.error("Got packet flooding warning message " + var1.a());
   }

   public void b(tB var1) {
      this.f = var1;
   }

   public String x() {
      if(this.v()) {
         StringBuilder var1 = new StringBuilder(this.i().h().a());
         var1.append("linkaccount?vaporsession=").append(this.f.i().q());
         return var1.toString();
      } else {
         return "http://apps.facebook.com/altitudegame/";
      }
   }
}
