
/** Set bots to use a random plane configuration */
class BotPlaneRandomCmd extends Command {
   private IntParam bot;

   public BotPlaneRandomCmd(fe game) {
      super(game, "botPlaneRandom", PermissionGroup.admin);
      this.bot = new IntParam("bot", "Bot Number (0 for all, -1 for default)");
      this.setParams(new JI[]{bot});
   }

   protected void execute(int[] a, String... b) {
      fN server = this.getGame().u().c();
      En bot_manager = server.n();
      bot_manager.setPlaneSetup(this.bot.value, null);
   }
}
