class AllowSpecChatCmd extends Command {
   GO allow;

   public AllowSpecChatCmd(fe game) {
      super(game, "allowSpectatorAllChat", PermissionGroup.admin);
      this.allow = new GO("allow", "allow");
      this.setParams(new JI[]{allow});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().setSpecAllChat((Boolean) this.allow.e());
   }
}
