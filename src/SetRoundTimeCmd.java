class SetRoundTimeCmd extends Command {
   private IntParam time;

   public SetRoundTimeCmd(fe game) {
      super(game, "setRoundTime", PermissionGroup.admin);
      this.time = new IntParam("time", "seconds * 30");
      this.setParams(new JI[]{time});
   }

   protected void execute(int[] a, String... b) {
      cV gameMode = this.getGame().T();

      if (gameMode instanceof bX) {
         bX oneLifeMode = (bX) gameMode;
         oneLifeMode.setRoundTime(oneLifeMode.getInitialRoundTime() - this.time.value);

         // Send a new GameModeStatusEv to all players
         fN server = this.getGame().u().c();
         VS gameMsgHandler = server.l();
         h gameModeEv = new h(this.getGame(), gameMsgHandler);
         gameMsgHandler.a(gameModeEv);
      }
   }
}
