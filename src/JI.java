
// Parameter
// - added getDescription()
public abstract class JI {
   protected static final wH[] b = new wH[0];
   private String name;
   private String description;

   public JI(String name, String description) {
      this.name = name;
      this.description = description;
   }

   public String c() {
      return this.name;
   }

   public String getDescription() {
      return this.description;
   }

   public void b(String var1) {
      this.name = var1;
   }

   public void c(String var1) {
      this.description = var1;
   }

   public abstract wH[] a(String var1);

   public abstract boolean a(NB var1, String var2);

   public String b() {
      return null;
   }
}
