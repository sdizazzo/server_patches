import mods.Config;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

public class NF extends Os {
   private static final Logger a = Logger.getLogger(NF.class);

   public NF(fe var1) {
      super("logPlanePositions", var1);
   }

   public String a() {
      return null;
   }

   public String b() {
      return null;
   }

   protected boolean a(NB var1, FW var2, Jt var3, String[] var4) {
      return true;
   }

   protected void a(int[] var1, String... var2) {
      fN var3 = this.j.u().c();
      HashMap var4 = new HashMap();

      for (Object o : var3.l().f()) {
         Jt client = (Jt) o;
         mN plane = client.h().i();

         int x = -1, y = -1, angle = -1;
         if (plane != null && plane.aO()) {
            x = Math.round(plane.f_().c);
            y = Math.round(plane.f_().d);
            angle = Math.round(plane.m());
         }

         String out = x + "," + y;
         if(Config.global.features.log_plane_angle){
            out += "," + angle;
         }

         var4.put(client.g(), out);
      }

      try {
         da var12 = JL.a("logPlanePositions");
         var12.a("positionByPlayer", (Map)var4);
         var3.a(var12);
      } catch (Exception var11) {
         a.error(var11, var11);
      }

   }
}
