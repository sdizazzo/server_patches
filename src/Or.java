import org.apache.log4j.Logger;

public class Or {
   private static final Logger LOG = Logger.getLogger(Or.class);
   private aaf b;
   private jV c;
   private oD d;
   private Fe e;
   private int f;
   public static final gB a = new js();

   private Or() {
   }

   public Or(aaf var1) {
      this(var1, jV.a, oD.c, mr.b(), 0);
   }

   public Or(aaf var1, jV var2, oD var3, Fe var4, int var5) {
      this();
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
   }

   public Or(Or var1, oD var2) {
      this(var1.b, var1.c, var2, var1.e, var1.f);
      //LOG.info("Updating team for player " + var1.b.E() + ", " + var1.d + " -> " + this.d);
   }

   public aaf a() {
      return this.b;
   }

   public jV b() {
      return this.c;
   }

   public oD c() {
      return this.d;
   }

   public Fe d() {
      return this.e;
   }

   public int e() {
      return this.f;
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(var1 == null) {
         return false;
      } else if(this.getClass() != var1.getClass()) {
         return false;
      } else {
         Or var2 = (Or)var1;
         return ty.a((Object)this.b, (Object)var2.b) && ty.a((Object)this.c, (Object)var2.c) && ty.a((Object)this.d, (Object)var2.d) && ty.a((Object)this.e, (Object)var2.e) && ty.a((Object)Integer.valueOf(this.f), (Object)Integer.valueOf(var2.f));
      }
   }

   // $FF: synthetic method
   Or(Or var1) {
      this();
   }

   // $FF: synthetic method
   static void a(Or var0, aaf var1) {
      var0.b = var1;
   }

   // $FF: synthetic method
   static void a(Or var0, jV var1) {
      var0.c = var1;
   }

   // $FF: synthetic method
   static void a(Or var0, oD var1) {
      var0.d = var1;
   }

   // $FF: synthetic method
   static void a(Or var0, Fe var1) {
      var0.e = var1;
   }

   // $FF: synthetic method
   static void a(Or var0, int var1) {
      var0.f = var1;
   }

   // $FF: synthetic method
   static aaf a(Or var0) {
      return var0.b;
   }

   // $FF: synthetic method
   static jV b(Or var0) {
      return var0.c;
   }

   // $FF: synthetic method
   static oD c(Or var0) {
      return var0.d;
   }

   // $FF: synthetic method
   static Fe d(Or var0) {
      return var0.e;
   }

   // $FF: synthetic method
   static int e(Or var0) {
      return var0.f;
   }
}
