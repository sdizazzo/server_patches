
/** Apply some force to the plane of a given player */
class ApplyForceCmd extends Command {
   private FloatParam x;
   private FloatParam y;
   private IntParam player_no;

   public ApplyForceCmd(fe game) {
      super(game, "applyForce", PermissionGroup.admin);
      this.player_no = new IntParam("player_no", "Player number");
      this.x = new FloatParam("x", "force X");
      this.y = new FloatParam("y", "force Y");
      this.setParams(new JI[]{player_no, x, y});
   }

   protected void execute(int[] a, String... b) {
      dp playerCont = this.getGame().J().u();
      mF player = playerCont.c(player_no.value);
      mN plane = player.i();

      fN server = this.getGame().u().c();
      VS gameMsgHandler = server.l();
      gN ev = new gN(this.getGame(), plane, x.value, y.value);
      gameMsgHandler.a(ev);
   }
}
