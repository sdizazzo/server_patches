import java.util.UUID;

class DenySpawnCmd extends Command {
   private IntParam player_no;
   private StringParam reason;

   public DenySpawnCmd(fe game) {
      super(game, "denySpawn", PermissionGroup.admin);
      this.player_no = new IntParam("playerNo", "Player number");
      this.reason = new StringParam("reason", "Reason");
      this.setParams(new JI[]{player_no, reason});
   }

   protected void execute(int[] a, String... b) {
      dp playerCont = this.getGame().J().u();
      mF player = playerCont.c(player_no.value);
      if(player != mF.c) {
         UUID vaporID = player.e().a().D();
         this.getServer().l().canSpawn.put(vaporID, false);
         if(this.reason.value != null) {
            this.getServer().l().spawnDenyReason.put(vaporID, reason.value);
         }
      }
   }
}
