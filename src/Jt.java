import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class Jt extends eA {
   private static final Logger a = Logger.getLogger(Jt.class);
   private final VS b;
   private lY c;
   private DC d;
   private int e;
   private int[] f = new int[1024];
   private Map g = new HashMap();
   private boolean h;
   private Ui i;
   private Xs j = new Xs(4);
   private GJ k = new GJ(31);
   private oD l;
   private int m;
   private float n = 0.14F;
   private float o = 3.0F;
   private List p = new ArrayList();
   private List q = new ArrayList();
   private final Yr r = new Yr();
   private final List s = new ArrayList();
   private bp t = new VI(this, (VI)null);
   private float u = 0.0F;
   private float v = 0.0F;
   private float w = 0.0F;
   private float x = 0.0F;
   private float y = 0.0F;
   private float z = 0.0F;
   public b level = null;

   public Jt(VS var1, lY var2, DC var3, int var4, b var5) {
      super((mj)null);
      this.b = var1;
      GG var6 = new GG(this, var1);
      mj var7 = new mj(var1.m(), var6);
      this.a((mj)var7);
      this.c = var2;
      this.d = var3;
      this.e = var4;
      if(var5 != null) {
         this.a((float)var5.c(), (float)var5.d());
      }
      this.level = var5;

   }

   public void a(kY var1) {
      this.a(this.b.m().a(var1));
   }

   protected void a(Zh var1) {
      this.a().a(var1);
   }

   public void a(boolean var1) {
      this.h = var1;
      if(var1) {
         this.m = this.b.n().H().a();
      }

   }

   public boolean d() {
      return this.h;
   }

   public void a(Ui var1) {
      this.i = var1;
   }

   public void b(Ui var1) {
      if(var1 != this.i) {
         this.i = var1;
         this.a((kY)var1);
      }

   }

   public void a(int var1, int var2) {
      this.g.put(Integer.valueOf(var1), Integer.valueOf(var2));
   }

   public DZ e() {
      return this.c.b();
   }

   public lY f() {
      return this.c;
   }

   public int g() {
      return this.e;
   }

   public mF h() {
      return this.b.n().J().u().c(this.g());
   }

   public void b(int var1, int var2) {
      this.r.a((float)var1, (float)var2);
   }

   private void s() {
      mN var3 = this.h().i();
      int var1;
      int var2;
      if(var3 != null && var3.aO()) {
         Yr var4 = var3.f_();
         this.r.a(var4);
         var1 = (int)var4.c;
         var2 = (int)var4.d;
      } else {
         var1 = Math.round(this.r.c);
         var2 = Math.round(this.r.d);
      }

      dp var9 = this.b.n().J().u();
      Kb var5 = var9.g().h();
      float var6 = var9.ak().e();
      int var7 = Math.round(var6 * 1280.0F);
      int var8 = Math.round(var6 * 720.0F);
      var5.a(this.s, var1, var2, var7, var8);
   }

   public boolean a(NG var1) {
      int var2 = this.s.size();

      for(int var3 = 0; var3 < var2; ++var3) {
         if(((NG)this.s.get(var3)).b(var1)) {
            return true;
         }
      }

      return false;
   }

   public String toString() {
      return "Client[name=" + this.f().E() + ", ip=" + this.e() + ", playerId=" + this.e + "]";
   }

   public Integer a(int var1) {
      return (Integer)this.g.get(Integer.valueOf(var1));
   }

   public void a(nQ var1, int var2) {
      this.a(var1, var2, false);
   }

   protected void a(nQ var1, int var2, boolean var3) {
      this.p.clear();
      int var4 = var1.b(10);

      int var6;
      for(int var5 = 0; var5 < var4; ++var5) {
         var6 = var1.b(10);
         if(var3) {
            var6 = this.a(var6).intValue();
         }

         this.p.add(Integer.valueOf(var6));
      }

      JB[] var10 = this.b.m().e();
      Iterator var7 = this.p.iterator();

      while(var7.hasNext()) {
         var6 = ((Integer)var7.next()).intValue();

         try {
            var10[var6].a(var1, var2);
         } catch (Exception var9) {
            a.error("[IP: " + this.e() + "] error reading pre-state for nuon " + var6, var9);
            break;
         }
      }

      Integer[] var11 = (Integer[])this.p.toArray(new Integer[this.p.size()]);
      this.q.add(new oi(var1, var2, var10, var11));
   }

   protected void b(nu var1) {
      int var2 = this.b.n().Y();
      JB[] var3 = this.b.m().e();
      this.a(var1, var2, var3);
   }

   private void a(nu var1, int var2, JB[] var3) {
      this.s();
      this.p.clear();
      int var4;
      if(this.d()) {
         for(var4 = 0; var4 < 1024; ++var4) {
            JB var5 = var3[var4];
            if(var5 != null) {
               int var6 = var2 - this.f[var4];
               if(var5.a(this, var6)) {
                  this.f[var4] = var2;
                  this.p.add(Integer.valueOf(var4));
               }
            }
         }
      }

      var1.b(10, this.p.size());
      Iterator var7 = this.p.iterator();

      while(var7.hasNext()) {
         var4 = ((Integer)var7.next()).intValue();
         var1.b(10, var4);
      }

      var7 = this.p.iterator();

      while(var7.hasNext()) {
         var4 = ((Integer)var7.next()).intValue();
         var3[var4].a(var1);
      }

      var7 = this.p.iterator();

      while(var7.hasNext()) {
         var4 = ((Integer)var7.next()).intValue();
         var3[var4].b(var1);
      }

   }

   public void c() {
      super.c();
      this.q.clear();
   }

   public void i() {
      try {
         Iterator var2 = this.q.iterator();

         while(var2.hasNext()) {
            oi var1 = (oi)var2.next();
            var1.a();
         }
      } finally {
         this.q.clear();
      }

   }

   public void j() {
      this.u = 0.0F;
   }

   public boolean k() {
      return this.h?this.b() > 150:this.u > 20000.0F;
   }

   public String l() {
      return this.h?this.b() + " updates written without response":this.u + " milliseconds since keep-alive received";
   }

   public void a(float var1) {
      if(!this.d()) {
         this.u += var1;
         this.v += var1;
         if(this.v > 2000.0F) {
            this.b.d().j().a(this.e());
            this.v = 0.0F;
         }
      }

      if(this.d()) {
         this.w += var1;
         if(this.w > 1000.0F) {
            this.w = 0.0F;
            this.b.d().k().a(this.e(), this.t);
         }

         if(!this.m()) {
            this.x += var1;
            if(this.x > 1000.0F) {
               this.x = 0.0F;
               fe var2 = this.b.n();
               int var3 = var2.H().a() - this.m;
               this.a((kY)(new a(var2, var3)));
            }
         }
      }

   }

   public boolean m() {
      return this.h().a();
   }

   public void a(oD var1) {
      this.l = var1;
   }

   public oD n() {
      return this.l;
   }

   public void a(DC var1) {
      this.d = var1;
   }

   public boolean o() {
      return this.d == null || this.d.d();
   }

   public boolean a(jV var1) {
      return this.o()?true:RR.a(var1.h()) && RR.a(var1.i()) && RR.a(var1.k()) && RR.a(var1.l());
   }

   public void a(float var1, float var2) {
      float var3 = 40.0F;
      float var4 = var1 + var2;
      if(var4 > var3) {
         float var5 = var3 / var4;
         var1 *= var5;
         var2 *= var5;
      }

      this.y = var1;
      this.z = var2;
   }

   public void p() {
      ++this.y;
   }

   public void q() {
      ++this.z;
   }

   public double r() {
      return this.m()?(double)(1000 - this.e().h()):(this.z == 0.0F?(double)this.y:(double)this.y / (double)this.z);
   }

   // $FF: synthetic method
   static float a(Jt var0) {
      return var0.o;
   }

   // $FF: synthetic method
   static float b(Jt var0) {
      return var0.n;
   }

   // $FF: synthetic method
   static void a(Jt var0, float var1) {
      var0.o = var1;
   }

   // $FF: synthetic method
   static Xs c(Jt var0) {
      return var0.j;
   }

   // $FF: synthetic method
   static VS d(Jt var0) {
      return var0.b;
   }

   // $FF: synthetic method
   static GJ e(Jt var0) {
      return var0.k;
   }

   static class VI implements bp {
      // $FF: synthetic field
      final Jt a;

      private VI(Jt var1) {
         this.a = var1;
      }

      public void a(double var1) {
         if(Jt.a(this.a) < 6.0F) {
            Jt.a(this.a, Math.min(Jt.a(this.a) + b(this.a), 6.0F));
         }

         boolean var3 = Math.abs(var1 - 999.0D) < 0.1D;
         if(var3 && Jt.a(this.a) >= 1.0F) {
            Jt.a(this.a, Jt.a(this.a) - 1.0F);
         } else {
            c(this.a).a(var1);
            int var4 = (int)var1;
            double var5 = c(this.a).c();
            if(var5 > 10.0D) {
               var4 += (int)var5;
            }

            this.a.h().a(var4);
            int var7 = d(this.a).d().m().m();
            if(var7 > 0) {
               int var8 = Math.round((float)var4);
               e(this.a).a(var8 > var7?1:0);
               if(e(this.a).b() > 15) {
                  e(this.a).e();
                  String var9 = "Kicked due to high ping. Your ping (" + var8 + ") exceeds the max ping (" + var7 + ") for this server.";
                  var9 = var9 + "\nTo lower your ping, close other network applications such as torrent downloaders.  If your ping is still too high, join another server or host your own server.";
                  d(this.a).a(this.a, var9, "ping kick");
               }
            }

         }
      }

      // $FF: synthetic method
      VI(Jt var1, VI var2) {
         this(var1);
      }
   }
}
