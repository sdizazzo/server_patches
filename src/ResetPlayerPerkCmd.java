import java.util.HashSet;
import java.util.UUID;

class ResetPlayerPerkCmd extends Command {
   private IntParam player_no;
   private StringParam perk_name;

   public ResetPlayerPerkCmd(fe game) {
      super(game, "resetPlayerPerk", PermissionGroup.admin);
      this.perk_name = new StringParam("perkName", "The name of the perk to reset");
      this.player_no = new IntParam("playerNo", "Player number");
      this.setParams(new JI[]{perk_name, player_no});
   }

   protected void execute(int[] a, String... b) {
      dp playerCont = this.getGame().J().u();
      mF player = playerCont.c(player_no.value);
      if(player != mF.c) {
         UUID vaporID = player.e().a().D();
         if (this.getServer().l().allowedPerks.containsKey(vaporID)) {
            this.getServer().l().allowedPerks.get(vaporID).remove(this.perk_name.value);
         }
      }
   }
}
