import org.apache.log4j.Logger;

import java.util.List;

// Bot Controller
// - added `setNextSetup`
public class UE extends Mg {
   private static final Logger a = Logger.getLogger(UE.class);
   private lO b;
   private Ek c;
   private Og d;
   private lf e;
   private lf f;
   private lf g;
   private int h = 0;
   private wj i;
   private float j;
   private float k;
   private float l;
   private int m;
   private UB n;
   private UB o;
   private int p;
   private float q;
   private float r;
   private float s;
   private float t;
   private rT u;
   private Hv v;
   private jV nextSetup;
   private static final float w = sx.d(3.0F).a();
   private int x;
   private int y;
   private int z = -1;
   private int A = -1;
   // $FF: synthetic field
   private static int[] B;

   public UE(fe var1, rT var2, int var3) {
      super(var1);
      this.u = var2;
      this.p = var3;
   }

   public void setNextSetup(jV setup) {
      this.nextSetup = setup;
   }

   public jV c() {
      if (this.nextSetup != null) {
         return this.nextSetup;
      }
      if (this.v != null) {
         tK var1 = (tK) this.v.a();
         int var2 = dh.b(((List) RR.f.get(var1)).size());
         if (var1 == tK.a && var2 == 2) {
            var2 = dh.b(2);
         }

         if (var1 == tK.d) {
            var2 = 2;
         }

         int var3 = 1 + dh.b(((List) RR.b.get(eX.c)).size() - 1);
         int var4 = 1 + dh.b(((List) RR.b.get(eX.d)).size() - 1);
         return new jV(var1, var2, var3, var4);
      } else {
         return jV.b;
      }
   }

   public void a(mN var1) {
      super.a(var1);
      float var2;
      float var3;
      switch (r()[this.u.ordinal()]) {
         case 2:
            this.n = new XU(0.0F, true, -28.0F, 28.0F, sx.d(1.5F));
            this.o = new XU(0.0F, true, -28.0F, 28.0F, sx.d(1.5F));
            var2 = 0.76F;
            var3 = 0.42F;
            break;
         case 3:
            this.n = new XU(0.0F, true, -10.0F, 10.0F, sx.d(1.5F));
            this.o = new XU(0.0F, true, -10.0F, 10.0F, sx.d(1.5F));
            var2 = 0.99F;
            var3 = 0.7F;
            break;
         case 4:
            this.n = new XU(0.0F, true, -3.0F, 3.0F, sx.d(1.5F));
            this.o = new XU(0.0F, true, -0.0F, 0.0F, sx.d(1.5F));
            var2 = 2.0F;
            var3 = 1.0F;
            break;
         default:
            a.warn("niUC: " + this.u);
         case 1:
            this.n = new XU(0.0F, true, -42.0F, 42.0F, sx.d(1.5F));
            this.o = new XU(0.0F, true, -42.0F, 42.0F, sx.d(1.5F));
            var2 = 0.52F;
            var3 = 0.15F;
            if (this.i() == 1 || this.i() == 2 || this.i() == 3) {
               var3 *= 0.7F;
            }
      }

      this.s = 1.0F / var3;
      this.r = var1.aq();
      this.q = var1.at();
      this.b = new lO(this);
      this.c = new Ek(this);
      this.d = new Og(this, var2);
      mN var4 = tK.a(this.b(), var1.R(), var1.aS(), var1.aU());
      this.e = this.a(var1.X(), var4.X());
      this.f = this.a(var1.Y(), var4.Y());
      this.g = new Ul(this);
      if (var1.bc().equals(tK.d)) {
         this.A = Math.round(sx.d(0.3F).a());
      }

   }

   public int i() {
      return this.p;
   }

   public rT j() {
      return this.u;
   }

   private lf a(bE var1, bE var2) {
      if (var1 instanceof ms) {
         if (var1 instanceof Yp) {
            var1 = ((Yp) var1).v();
            var2 = ((Yp) var2).v();
         }

         Jp var3 = ((ms) var2).a();
         return (lf) (var1 instanceof Jv ? new Zp(this, this, (ms) var1, var3.A().c(), 15.0F) : (var1 instanceof br ? new gM(this, this, (br) var1, var3.A().c(), 22.0F) : (var1 instanceof Xj ? new ka(this, this, (Xj) var1, var3.A().c(), 25.0F) : (var1 instanceof JF ? new mX(this, (ms) var1, var3.A().c(), 25.0F) : (var1 instanceof EE ? new Zv(this, this, (ms) var1, var3.A().c(), 15.0F) : (var1 instanceof bU ? new Zt(this, this, (ms) var1, var3.A().c(), 15.0F) : (var1 instanceof li ? new mX(this, (ms) var1, var3.A().c(), 5.0F) : (var1 instanceof Jo ? new ZB(this, this, (ms) var1, var3.A().c(), 0.0F) : (var1 instanceof kk ? new mX(this, (ms) var1, var3.A().c(), 38.0F) : new mX(this, (ms) var1, var3.A().c(), 15.0F))))))))));
      } else if (var1 instanceof Dj) {
         return new Zz(this, this);
      } else if (var1 instanceof rQ) {
         return new ZD(this, this);
      } else {
         a.warn("noabm4: " + var1);
         return new lf(this);
      }
   }

   public void g() {
      if (this.m > 0) {
         --this.m;
      }

      this.n.b();
      this.o.b();
      this.j = dh.a(1.0F);
      this.i = this.b().J().u().g().o();
      this.d();
      this.b.b();
      this.c.a();
      this.k = this.c.e();
      this.l = this.o.b(0.0F);
      this.s();
      if (!this.a((Am) Am.e) && !this.a((Am) Am.f)) {
         ++this.h;
      } else {
         this.h = 0;
      }

      this.d.a(this.k, this.h);
      if (this.k()) {
         mN var1 = this.a();
         this.a(Am.a, false);
         this.a(Am.b, false);
         if (!var1.aR().m()) {
            float var2 = dh.b(this.k - var1.m(), -180.0F, 180.0F);
            if (Math.abs(var2) > 8.0F) {
               this.a(var2 > 0.0F ? Am.a : Am.b, true);
            }
         }

         this.a(Am.c, false);
         if (!var1.ai()) {
            this.a(Am.d, true);
         }
      } else if (this.c.d()) {
         float var3 = dh.b(this.k - this.a().m(), -180.0F, 180.0F);
         this.a(Am.a, false);
         this.a(Am.b, false);
         if (Math.abs(var3) > 8.0F) {
            this.a(var3 > 0.0F ? Am.a : Am.b, true);
         }
      }

   }

   private void s() {
      mN var1 = this.a();
      float var2 = var1.aq();
      float var3 = var2 - this.t;
      if (var3 == 0.0F && this.r < var2) {
         var3 = var1.as();
      } else if (var3 < 0.0F) {
         var3 *= this.s;
      }

      this.r = dh.a(this.r + var3, -this.q / 2.0F, this.q);
      this.t = var2;
      float var4;
      bE var5;
      ms var6;
      boolean var7;
      if (this.e.a()) {
         var4 = 0.0F;
         var5 = var1.X();
         if (var5 instanceof Jo) {
            if (this.r < 0.0F && !((TG) var1).B()) {
               var4 = 9999.0F;
            } else {
               var4 = -9999.0F;
            }
         } else if (var5 instanceof ms) {
            var6 = (ms) var5;
            var4 = var6.l();
            if (!var6.k().e()) {
               this.x = 0;
            } else if (var1.aq() < var4) {
               ++this.x;
            }
         }

         if (this.r >= var4) {
            var7 = this.A > 0 && this.y - this.x >= this.A;
            if (!var7) {
               this.a(Am.e, true);
            }
         }
      } else if (this.x > 0) {
         --this.x;
      }

      if (this.f.a()) {
         var4 = 0.0F;
         var5 = var1.Y();
         if (var5 instanceof ms) {
            var6 = (ms) var5;
            var4 = var6.l();
            if (!var6.k().e()) {
               this.y = 0;
            } else if (var1.aq() < var4) {
               ++this.y;
            }
         }

         if (this.r >= var4) {
            var7 = this.z > 0 && this.x - this.y >= this.z;
            if (!var7) {
               this.a(Am.f, true);
               if (var1.bc().equals(tK.e)) {
                  switch (r()[this.u.ordinal()]) {
                     case 1:
                        this.m = Math.round(3.3F * sx.a.a());
                        break;
                     case 2:
                        this.m = Math.round(2.4F * sx.a.a());
                        break;
                     case 3:
                        this.m = Math.round(1.0F * sx.a.a());
                        break;
                     case 4:
                        this.m = 0;
                  }
               }
            }
         }
      } else if (this.y > 0) {
         --this.y;
      }

      if (this.g.a()) {
         this.a(Am.g, true);
      }

   }

   public boolean k() {
      return this.c.c();
   }

   public boolean l() {
      qW var1 = this.c.b();
      if (var1 instanceof SR) {
         SR var2 = (SR) var1;
         if (!var2.L() && (var2 instanceof Jg || var2 instanceof tc)) {
            return true;
         }
      }

      return false;
   }

   public float m() {
      return this.n.b(0.0F);
   }

   public float n() {
      return this.l;
   }

   public lO o() {
      return this.b;
   }

   public boolean a(float var1, float var2) {
      return this.b(var1, var2) >= var2;
   }

   public float b(float var1, float var2) {
      mN var3 = this.a();
      return this.i.a(var3.aA(), var3.f_(), var1, var2);
   }

   public wj p() {
      return this.i;
   }

   public boolean h() {
      return true;
   }

   // $FF: synthetic method
   static rT a(UE var0) {
      return var0.u;
   }

   // $FF: synthetic method
   static float b(UE var0) {
      return var0.j;
   }

   // $FF: synthetic method
   static float q() {
      return w;
   }

   // $FF: synthetic method
   static int[] r() {
      int[] var10000 = B;
      if (B != null) {
         return var10000;
      } else {
         int[] var0 = new int[rT.values().length];

         try {
            var0[rT.a.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[rT.d.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[rT.c.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            var0[rT.b.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

         B = var0;
         return var0;
      }
   }

   // $FF: synthetic method
   static int c(UE var0) {
      return var0.m;
   }

   // $FF: synthetic method
   static float d(UE var0) {
      return var0.k;
   }
}
