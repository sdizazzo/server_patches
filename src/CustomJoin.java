import mods.Config;
import org.apache.log4j.Logger;

public class CustomJoin {
   private static Logger logger = Logger.getLogger(CustomJoin.class);

   // Returns null to allow the join, otherwise returns the error
   // message to be sent to the player as a string.
   // The last parameter is the result of the original checker,
   // which checks the version, password, level requirements
   // and checks if the user is banned.
   static String checkJoin(JoinReqInfo info, int port, String orig) {
      if (!Config.get(port).custom_join.ignore_defaults) {
         if (orig != null) return orig;
      }
      // If the user is a bot allow them to join
      if (info.vaporID.equals(mF.a)) return null;

      // Try our custom join handlers, which may accept or deny the request:
      String restrict_level = CustomJoin.restrict_level(info, port);
      if (restrict_level != null) return restrict_level;

      String whitelist = CustomJoin.whitelist(info, port);
      if (whitelist != null) return whitelist;

      return null;
   }

   private static String restrict_level(JoinReqInfo info, int port) {
      try {
         Config.RestrictLevel conf = Config.get(port).restrict_level;
         if (!conf.enabled) return null;

         // The simple case, only one Ace is allowed, usually ace 0:
         if (conf.min.ace == conf.max.ace) {
            if (info.ace == conf.min.ace) {
               if (info.level >= conf.min.level && info.level <= conf.max.level)
                  return null;
               else
                  return "You must be between level " + conf.min.level + " and " +
                          conf.max.level + " to join this server.";
            } else {
               return "You must be Ace " + conf.min.ace + " to join this server.";
            }
         }

         if (info.ace > conf.min.ace && info.ace < conf.max.ace) return null;
         if (info.ace == conf.min.ace && info.level >= conf.min.level) return null;
         if (info.ace == conf.max.ace && info.level <= conf.max.level) return null;

         return "You do not meet the level requirements for this server";
      } catch (Exception e) {
         logger.error("Failed to parse restrict_level config: " + e.toString());
         return null;
      }
   }

   // A simple whitelist.
   private static String whitelist(JoinReqInfo info, int port) {
      try {
         Config.Whitelist conf = Config.get(port).whitelist;
         if (!conf.enabled) return null;

         String req_id = info.vaporID.toString();
         for (String s : conf.allowed) {
            if (s.equals(req_id)) return null;
         }

         return "Only whitelisted players can join this server right now, sorry!";
      } catch (Exception e) {
         logger.error("Failed to parse whitelist config: " + e.toString());
         return null;
      }
   }
}
