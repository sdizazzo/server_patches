import org.apache.log4j.Logger;

class RemoveBotCmd extends Command {
   private static final Logger logger = Logger.getLogger(RemoveBotCmd.class);

   public RemoveBotCmd(fe game) {
      super(game, "removeBot", PermissionGroup.admin);
      this.setParams(new JI[]{});
   }

   protected void execute(int[] a, String... b) {
      fN server = this.getGame().u().c();
      En bot_manager = server.n();
      bot_manager.removeBot();
   }
}
