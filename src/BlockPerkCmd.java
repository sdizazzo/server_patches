class BlockPerkCmd extends Command {
   private StringParam perk_name;
   private StringParam reason;

   public BlockPerkCmd(fe game) {
      super(game, "blockPerk", PermissionGroup.admin);
      this.perk_name = new StringParam("perkName", "The name of the perk to block");
      this.reason = new StringParam("reason", "The reason to give when denying a spawn request");
      this.setParams(new JI[]{perk_name, reason});
   }

   protected void execute(int[] a, String... b) {
      this.getServer().l().blockedPerks.put(perk_name.value, reason.value);
   }
}
