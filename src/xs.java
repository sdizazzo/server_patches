import java.util.*;

public class xs {
   public List s = new ArrayList();
   public LV a = new LV(this, "Kills", 0, -128, 895);
   public LV b = new LV(this, "Deaths", 0, 0, 1023);
   public LV c = new LV(this, "Crashes", 0, 0, 1023);
   public LV d = new LV(this, "Assists", 0, 0, 1023);
   public EL e = new EL(this, "Damage Dealt", 0.0F, -2000.0F, 120000.0F, 0.1F);
   public EL f = new EL(this, "Damage Received", 0.0F, -2000.0F, 120000.0F, 0.1F);
   public EL g = new EL(this, "Damage Dealt to Enemy Buildings", 0.0F, 0.0F, 65535.0F, 1.0F);
   public LV h = new LV(this, "Longest Life", 0, 0, 511);
   public LV i = new LV(this, "Kill Streak", 0, 0, 127);
   public LV j = new LV(this, "Multikill", 0, 0, 15);
   public LV k = new LV(this, "Experience", 0, 0, 16383);
   public LV l = new LV(this, "Goals Scored", 0, 0, 63);
   public LV m = new LV(this, "Goals Assisted", 0, 0, 63);
   public LV n = new LV(this, "Ball Possession Time", 0, 0, 511);
   public LV o = new LV(this, "Kills", 0, -128, 895);
   public LV p = new LV(this, "Assists", 0, 0, 1023);
   public LV q = new LV(this, "Deaths", 0, 0, 1023);
   private List t = new ArrayList();
   public List r;

   public xs() {
      this.t.add(this.o);
      this.t.add(this.p);
      this.t.add(this.q);
      this.r = new ArrayList(this.s);
      this.r.removeAll(this.t);
   }

   public Map<String, Number> getThisLife() {
      Map<String, Number> map = new HashMap<>();
      for (Object o : r) {
         SX stat = (SX) o;
         map.put(stat.b(), stat.getThisLife());
      }
      return map;
   }

   public void clearThisLife() {
      for (Object o : r) {
         SX stat = (SX) o;
         stat.setThisLife(0);
      }
   }

   public boolean a() {
      return this.has_changed(this.t);
   }

   public void b() {
      SX var1;
      for (Iterator var2 = this.t.iterator(); var2.hasNext(); var1.changed = false) {
         var1 = (SX) var2.next();
      }
   }

   public void a(nu msg_out) {
      this.a(this.r, false, msg_out);
   }

   public void a(nQ msg_in) {
      this.a(this.r, false, msg_in);
   }

   public void a(nu msg_out, boolean diff) {
      this.a(this.t, diff, msg_out);
   }

   public void a(nQ msg_in, boolean diff) {
      this.a(this.t, diff, msg_in);
   }

   private boolean has_changed(List var1) {
      for (int var2 = 0; var2 < var1.size(); ++var2) {
         if (((SX) var1.get(var2)).changed) {
            return true;
         }
      }

      return false;
   }

   private void a(List var1, boolean var2, nQ var3) {
      for (int var4 = 0; var4 < var1.size(); ++var4) {
         boolean var5 = true;
         if (var2) {
            var5 = var3.d();
         }

         if (var5) {
            SX var6 = (SX) var1.get(var4);
            var6.a(var3);
         }
      }

   }

   private void a(List var1, boolean var2, nu var3) {
      for (int var4 = 0; var4 < var1.size(); ++var4) {
         SX var5 = (SX) var1.get(var4);
         boolean var6 = true;
         if (var2) {
            var6 = var5.changed;
            var3.a(var6);
         }

         if (var6) {
            var5.a(var3);
         }
      }

   }

}
