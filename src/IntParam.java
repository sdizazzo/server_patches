
/** An integer parameter to a server command*/
class IntParam extends JI {
   public int value;

   IntParam(String name, String description) {
      super(name, description);
   }

   public wH[] a(String var1) {
      return null;
   }

   public boolean a(NB var1, String str) {
      try {
         this.value = Integer.parseInt(str);
         return true;
      } catch (Exception e) {
         var1.a("Parameter " + this.c() + " must be an integer", false);
         return false;
      }
   }
}
