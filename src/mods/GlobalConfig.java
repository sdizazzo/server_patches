package mods;

import org.hjson.JsonObject;
import org.hjson.JsonValue;

import java.util.ArrayList;
import java.util.List;

public class GlobalConfig {
   public String vapor_keystore = "vapor/VaporClientTrust.jks";
   public String vapor_address = "vapor.nimblygames.com";
   public List<String> client_commands = new ArrayList<>();
   public boolean fix_commands_latency = true;
   public Features features = new Features();

   public GlobalConfig() {}
   public GlobalConfig(JsonValue json) {
      if(json != null && json.isObject()) {
         JsonObject obj = json.asObject();
         vapor_keystore = obj.getString("vapor_keystore", vapor_keystore);
         vapor_address = obj.getString("vapor_address", vapor_address);
         client_commands = Config.getList(obj.get("client_commands"));
         fix_commands_latency = obj.getBoolean("fix_commands_latency", fix_commands_latency);
         features = new Features(obj.get("features"));
      }
   }

   public class Features {
      public boolean log_plane_angle = false;
      private Features() {}
      public Features(JsonValue json){
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            log_plane_angle = obj.getBoolean("log_plane_angle", log_plane_angle);
         }
      }
   }

}
