package mods;

import org.apache.log4j.Logger;
import org.hjson.JsonObject;
import org.hjson.JsonValue;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Config {
   private static Logger log = Logger.getLogger(Config.class);
   private static final File serversFile = new File("servers");
   public static GlobalConfig global = new GlobalConfig();
   private static Config standard = new Config();
   private static Map<Integer, Config> servers = new HashMap<>();

   public static Config get(int port) {
      if (servers.containsKey(port)) return servers.get(port);
      else return standard;
   }

   public static void load() {
      File f = new File(serversFile, "mods.hjson");
      if (f.exists()) {
         try {
            log.info("Loading mod config file " + f.getPath());
            byte[] encoded = Files.readAllBytes(f.toPath());
            String data = new String(encoded, Charset.defaultCharset());
            JsonObject root = JsonValue.readHjson(data).asObject();

            Config.global = new GlobalConfig(root.get("global"));
            Config.servers.clear();

            JsonObject servers = root.get("servers").asObject();
            Config.standard = new Config(servers.get("default"), null);

            for (JsonObject.Member m : root.get("servers").asObject()) {
               if (!m.getName().equals("default")) {
                  int port = Integer.parseInt(m.getName());
                  Config.servers.put(port, new Config(m.getValue(), Config.standard));
               }
            }
            log.info("Mods configuration loaded");
         } catch (Exception e) {
            log.error("Error reading mods config: " + e.toString());
         }
      } else {
         log.error("Error: Mod config file " + f.getPath() + " doesn't exist");
      }
   }

   public boolean auto_stop_tournament = true;
   public boolean tournament_team_chat = false;
   public boolean allow_spawn = true;
   public String deny_spawn_reason = "^WHITE^You can't spawn right now.";
   public boolean ffa_allow_assign_team = false;
   public boolean config_size_workaround = false;
   public float position_log_time = 0.0f;
   public int ping_log_time = 5;
   public int vote_cooldown = 90000;
   public int vote_fail_cooldown = 300000;
   public Permissions permissions = new Permissions();
   public Bots bots = new Bots();
   public CustomJoin custom_join = new CustomJoin();
   public RestrictLevel restrict_level = new RestrictLevel();
   public Whitelist whitelist = new Whitelist();

   public Config() {}
   public Config(JsonValue json, Config base) {
      if(base != null) {
         auto_stop_tournament = base.auto_stop_tournament;
         tournament_team_chat = base.tournament_team_chat;
         allow_spawn = base.allow_spawn;
         deny_spawn_reason = base.deny_spawn_reason;
         ffa_allow_assign_team = base.ffa_allow_assign_team;
         config_size_workaround = base.config_size_workaround;
         position_log_time = base.position_log_time;
         ping_log_time = base.ping_log_time;
         vote_cooldown = base.vote_cooldown;
         vote_fail_cooldown = base.vote_fail_cooldown;
         permissions = base.permissions;
         bots = base.bots;
         custom_join = base.custom_join;
         restrict_level = base.restrict_level;
         whitelist = base.whitelist;
      }
      if(json != null && json.isObject()) {
         JsonObject obj = json.asObject();
         auto_stop_tournament = obj.getBoolean("auto_stop_tournament", auto_stop_tournament);
         tournament_team_chat = obj.getBoolean("tournament_team_chat", tournament_team_chat);
         allow_spawn = obj.getBoolean("allow_spawn", allow_spawn);
         deny_spawn_reason = obj.getString("deny_spawn_reason", deny_spawn_reason);
         ffa_allow_assign_team = obj.getBoolean("ffa_allow_assign_team", ffa_allow_assign_team);
         config_size_workaround = obj.getBoolean("config_size_workaround", config_size_workaround);
         position_log_time = obj.getFloat("position_log_time", position_log_time);
         ping_log_time = obj.getInt("ping_log_time", ping_log_time);
         vote_cooldown = obj.getInt("vote_cooldown", vote_cooldown);
         vote_fail_cooldown = obj.getInt("vote_fail_cooldown", vote_fail_cooldown);
         permissions = new Permissions(obj.get("permissions"), permissions);
         bots = new Bots(obj.get("bots"), bots);
         custom_join = new CustomJoin(obj.get("custom_join"), custom_join);
         restrict_level = new RestrictLevel(obj.get("restrict_level"), restrict_level);
         whitelist = new Whitelist(obj.get("whitelist"), whitelist);
      }
   }

   public class Permissions {
      public boolean enabled = false;
      public boolean fallback = true;
      public String file_name = "";

      Permissions() {}
      public Permissions(JsonValue json, Permissions base) {
         enabled = base.enabled;
         fallback = base.fallback;
         file_name = base.file_name;
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            enabled = obj.getBoolean("enabled", enabled);
            fallback = obj.getBoolean("fallback", fallback);
            file_name = obj.getString("file_name", file_name);
         }
      }
   }

   public class Bots {
      public boolean allow_join_tournament = false;
      public boolean allow_spawn_tournament = false;
      public boolean commands_target_bots = false;
      public boolean prevent_team_change = false;
      public String name_prefix = "Bot ";
      public List<String> names = new ArrayList<>();

      Bots() {}
      public Bots(JsonValue json, Bots base) {
         allow_join_tournament = base.allow_join_tournament;
         allow_spawn_tournament = base.allow_spawn_tournament;
         commands_target_bots = base.commands_target_bots;
         prevent_team_change = base.prevent_team_change;
         name_prefix = base.name_prefix;
         names = base.names;
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            allow_join_tournament = obj.getBoolean("allow_join_tournament", allow_join_tournament);
            allow_spawn_tournament = obj.getBoolean("allow_spawn_tournament", allow_spawn_tournament);
            commands_target_bots = obj.getBoolean("commands_target_bots", commands_target_bots);
            prevent_team_change = obj.getBoolean("prevent_team_change", prevent_team_change);
            name_prefix = obj.getString("name_prefix", name_prefix);
            names = getList(obj.get("names"));
         }
      }
   }

   public class CustomJoin {
      public boolean ignore_defaults = false;

      CustomJoin() {}
      public CustomJoin(JsonValue json, CustomJoin base) {
         ignore_defaults = base.ignore_defaults;
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            ignore_defaults = obj.getBoolean("ignore_defaults", ignore_defaults);
         }
      }
   }

   public class RestrictLevel {
      public boolean enabled = false;
      public Level min = new Level();
      public Level max = new Level();

      public class Level {
         public int ace = 0;
         public int level = 0;
         Level() {}
         public Level(JsonValue json, Level base) {
            ace = base.ace;
            level = base.level;
            if(json != null && json.isObject()) {
               JsonObject obj = json.asObject();
               ace = obj.getInt("ace", ace);
               level = obj.getInt("level", level);
            }
         }
      }

      RestrictLevel() {}
      RestrictLevel(JsonValue json, RestrictLevel base) {
         enabled = base.enabled;
         min = base.min;
         max = base.max;
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            enabled = obj.getBoolean("enabled", enabled);
            min = new Level(obj.get("min"), min);
            max = new Level(obj.get("max"), min);
         }
      }
   }

   public class Whitelist {
      public boolean enabled = false;
      public List<String> allowed = new ArrayList<>();

      Whitelist() {}
      Whitelist(JsonValue json, Whitelist base) {
         enabled = base.enabled;
         allowed = base.allowed;
         if(json != null && json.isObject()) {
            JsonObject obj = json.asObject();
            enabled = obj.getBoolean("enabled", enabled);
            allowed = getList(obj.get("allowed"));
         }
      }

   }

   static List<String> getList(JsonValue json) {
      List<String> list = new ArrayList<>();
      if(json != null && json.isArray()) {
         for(JsonValue value : json.asArray()) {
            if(value != null && value.isString()) list.add(value.asString());
         }
      }
      return list;
   }
}
