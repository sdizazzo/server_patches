import org.apache.log4j.Logger;
import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;

public class ServerPatches {
   private static final Logger logger = Logger.getLogger(ServerPatches.class);

   static {
      mods.Config.load();

      Commands.register_command(ReloadCfgCmd.class);
      Commands.register_command(ReloadPermissionsCmd.class);
      Commands.register_command(BotPlaneSetupCmd.class);
      Commands.register_command(BotPlaneRandomCmd.class);
      Commands.register_command(OverrideTDMScoreCmd.class);
      Commands.register_command(SetTimeCmd.class);
      Commands.register_command(SetRoundTimeCmd.class);
      Commands.register_command(EndRoundCmd.class);
      Commands.register_command(GetTimeCmd.class);
      Commands.register_command(ApplyForceCmd.class);
      Commands.register_command(ResetBallCmd.class);
      Commands.register_command(ResetBallTeamCmd.class);
      Commands.register_command(AddBotCmd.class);
      Commands.register_command(RemoveBotCmd.class);
      Commands.register_command(AllowSpecChatCmd.class);
      Commands.register_command(SetSpecChatCmd.class);
      Commands.register_command(AllowSpawnCmd.class);
      Commands.register_command(DenySpawnCmd.class);

      Commands.register_command(BlockPerkCmd.class);
      Commands.register_command(UnblockPerkCmd.class);
      Commands.register_command(AllowPlayerPerkCmd.class);
      Commands.register_command(ResetPlayerPerkCmd.class);
      Commands.register_command(ResetPerksCmd.class);
   }

   static da make_log() {
      da log = JL.a("serverPatches");

      String version = "undefined";
      String fileName = "version.properties";

      Properties p = new Properties();
      try(InputStream resourceStream = ServerPatches.class.getResourceAsStream(fileName)) {
         if(resourceStream != null) {
            p.load(resourceStream);
         } else {
            logger.warn("Unable to load " + fileName + "file");
         }
      } catch(IOException e) {
         // ignore exception during resourceStream.close()
      }

      version = p.getProperty("version", version);
      log.a("version", version);

      return log;
   }
}
