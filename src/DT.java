import org.apache.log4j.Logger;

public class DT extends kY {
   private static final Logger log = Logger.getLogger(DT.class);
   private int playerNo;
   private boolean team;
   private String message;

   public DT(fe var1) {
      super(var1);
   }

   public DT(fe game, int var2, boolean var3, String var4) {
      this(game);
      this.playerNo = var2;
      this.team = var3;
      this.message = var4;
   }

   public boolean c() {
      boolean allow = super.c();
      int playerNo = this.playerNo;
      Jt client = this.d();
      if(client != null) {
         allow &= client.g() == this.playerNo;
         this.message = this.message.replaceAll("\\s{3,}+", "   ");
      }

      if (!this.e().u().c().allowAllChat(playerNo)) {
         this.team = true;
      }

      boolean serverMsg = this.message.startsWith(">") && this.e().ao().a(client).c().equals(YY.b);
      if(serverMsg) {
         this.playerNo = -1;
         this.message = this.message.substring(1);
         this.team = false;
      }

      boolean blocked = false;
      if(client != null) {
         boolean all_chat = !this.team;
         long expires = 0L;
         String blockReason = null;
         qo bans = this.e().u().c().m().R();
         eF ipBan = bans.a(client.e().i(), all_chat);
         eF vaporBan = bans.a(client.f().D(), all_chat);

         if(ipBan != null && ipBan.d() > expires) {
            expires = ipBan.d();
            blockReason = ipBan.f();
         }
         if(vaporBan != null && vaporBan.d() > expires) {
            expires = vaporBan.d();
            blockReason = vaporBan.f();
         }

         long time = System.currentTimeMillis();
         if(expires > time) {
            long remaining = expires - time;
            String str = blockReason + "; block expires in " + me.a(remaining, true) + ".";
            client.a((kY)(new DT(this.e(), -1, false, str)));
            blocked = true;
            allow = false;
         }
      }

      try {
         da log = JL.a("chat");
         log.a("player", playerNo);
         log.a("message", (Object)this.message);
         log.a("server", this.playerNo == -1);
         log.a("team", this.team);
         log.a("blocked", blocked);
         this.e().u().c().a(log);
      } catch (Exception e) {
         log.error(e, e);
      }

      return allow;
   }

   public boolean a(Jt client) {
      return this.e().u().c().shouldForwardChat(this.playerNo, client.h().h(), this.team);
   }

   public void a() {
      if(this.e().F()) {
         this.f().a(this.playerNo, this.team, this.message);
      }
   }

   public void a(nQ var1) {
      this.playerNo = Vh.b(var1);
      this.team = var1.d();
      this.message = var1.j();
   }

   public void a(nu var1) {
      Vh.a(var1, this.playerNo);
      var1.a(this.team);
      var1.a(this.message);
   }
}
