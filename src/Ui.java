import mods.Config;
import org.apache.log4j.Logger;

import java.io.IOException;

public class Ui extends kY {
   private static final Logger log = Logger.getLogger(Ui.class);
   private static final YQ b = new YQ(512);
   private boolean c;
   private byte[] d;

   public Ui(fe var1) {
      super(var1);
   }

   public Ui(fe var1, VS var2, boolean minimal) {
      this(var1);

      try {
         this.d = sT.a(var2, minimal);
         byte[] var3 = ic.a(this.d, b);
         if (var3.length < this.d.length) {
            this.c = true;
            this.d = var3;
         }
      } catch (Exception var4) {
         log.error(var4, var4);
      }

      if ((minimal || !Config.get(var1.u().c().port()).config_size_workaround) && this.d.length > 1450) {
         throw new RuntimeException("\n\n\nServer config size (" + this.d.length + " bytes) " +
                 "exceeds max allowed size (" + 1450 + " bytes).\n" +
                 "Reduce the server config size by removing maps from the map list and map rotation.\n" +
                 "Trim the config using the Server Configurator tool or by manually editing launcher_config.xml\n" +
                 "\n\n");
      }
   }

   public void a() {
      try {
         byte[] var1 = this.c ? ic.a(this.d, b.c) : this.d;
         sT var2 = new sT(this.e());
         var2.a(var1);
         this.e().J().u().a(var2);
      } catch (Exception var3) {
         log.error(var3, var3);
      }

   }

   public void a(nQ var1) {
      this.c = var1.d();
      int var2 = var1.b(12);
      this.d = new byte[var2];
      try {
         var1.read(this.d);
      } catch (IOException e) {
         log.error("Failed to read ServerConfig", e);
      }
   }

   public void a(nu var1) {
      var1.a(this.c);
      var1.b(12, this.d.length);
      try {
         var1.write(this.d);
      } catch (IOException e) {
         log.error("Failed to write ServerConfig", e);
      }
   }
}
