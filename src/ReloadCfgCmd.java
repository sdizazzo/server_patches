
/** Reload the `mods.hjson` configuration file */
class ReloadCfgCmd extends Command {
   public ReloadCfgCmd(fe game) {
      super(game, "reloadModConfig", PermissionGroup.admin);
      this.setParams(new JI[]{});
   }

   protected void execute(int[] a, String... b) {
      mods.Config.load();
   }
}
