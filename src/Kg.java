import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

// Command Manager
// - Add new commands to the main collection.
// - Make vote cooldown configurable.
public class Kg {
   static final Logger a = Logger.getLogger(Kg.class);
   private static final Comparator d = new Ga();
   private long e = 15000L;
   private long f = 2000L;
   private String[] g = new String[]{"Yes", "No"};
   private int[] h;
   private fe i;
   private List j;
   private List k;
   private List l;
   private List m;
   private List n;
   private Map o;
   private Map p;
   private hp q;
   private Map r;
   private boolean s;
   private Map t;
   private long u;
   private int v;
   Os b;
   private String[] w;
   private boolean x;
   private long y;
   private FW z;
   private Map A;
   private Map B;
   private Set C;
   public static final FW c;

   static {
      lY var0 = new lY();
      var0.b("Server");
      var0.a(new UUID(0L, 0L));
      var0.b(Long.valueOf(0L));
      var0.c(new UUID(0L, 0L));
      c = new FW(var0);
      c.a(YY.b);
   }

   public Kg(fe var1) {
      this.h = new int[this.g.length];
      this.j = new ArrayList();
      this.k = new ArrayList();
      this.l = new ArrayList();
      this.m = new ArrayList();
      this.n = new ArrayList();
      this.o = new HashMap();
      this.p = new HashMap();
      this.r = new HashMap();
      this.t = new HashMap();
      this.A = new HashMap();
      this.B = new HashMap();
      this.C = new HashSet();
      this.i = var1;
      this.setupCommands(true);
   }

   public Kg(fe var1, boolean var2) {
      this.h = new int[this.g.length];
      this.j = new ArrayList();
      this.k = new ArrayList();
      this.l = new ArrayList();
      this.m = new ArrayList();
      this.n = new ArrayList();
      this.o = new HashMap();
      this.p = new HashMap();
      this.r = new HashMap();
      this.t = new HashMap();
      this.A = new HashMap();
      this.B = new HashMap();
      this.C = new HashSet();
      this.i = var1;
      this.setupCommands(var2);
   }

   public fe a() {
      return this.i;
   }

   public lj a(String var1) {
      lj var2 = (lj) this.o.get(var1);
      if (var2 == null) {
         lj[] var3 = this.c(var1);
         if (var3.length == 1) {
            var2 = var3[0];
         } else {
            lj[] var7 = var3;
            int var6 = var3.length;

            for (int var5 = 0; var5 < var6; ++var5) {
               lj var4 = var7[var5];
               if (var4.f().equalsIgnoreCase(var1)) {
                  var2 = var4;
                  break;
               }
            }
         }
      }

      return var2;
   }

   private void setupCommands(boolean var1) {
      this.j.add(new MZ(this.i));
      this.j.add(new Fh(this.i));
      this.j.add(new CM(this.i));
      this.j.add(new Jy(this.i));
      this.j.add(new Ho(this.i));
      this.j.add(new VK(this.i));
      this.j.add(new HH(this.i));
      Collections.sort(this.j, d);

      // Add the commands defined in `Command` to the main command list.
      for (lj c : Commands.get_commands(this.a())) {
         this.k.add(c);
      }
      this.k.add(new yi(this.i));
      this.k.add(new io(this.i));
      this.k.add(new Fx(this.i));
      this.k.add(new NF(this.i));
      this.k.add(new cW(this.i));
      this.k.add(new za(this.i));
      this.k.add(new Ty(this.i));
      this.k.add(new ca(this.i));
      this.k.add(new hJ(this.i));
      this.k.add(new Yx(this.i));
      this.k.add(new nh(this.i));
      this.k.add(new f(this.i));
      this.k.add(new uz(this.i));
      this.k.add(new ed(this.i));
      this.k.add(new Xu(this.i));
      this.k.add(new IE(this.i));
      this.k.add(new wF(this.i));
      this.k.add(new bm(this.i));
      this.k.add(new sv(this.i, this));
      this.k.add(new FN(this.i));
      this.k.add(new hL(this.i));
      this.k.add(new HW(this.i));
      this.k.add(new WS(this.i));
      this.k.add(new OL(this.i));
      this.k.add(new Nf(this.i));
      this.k.add(new ap(this.i));
      this.k.add(new vZ(this.i));
      this.k.add(new zm(this.i));
      this.k.add(new Ld(this.i));
      this.k.add(new FY(this.i));
      Collections.sort(this.k, d);
      if (var1) {
         this.m.add(new HJ(this.i));
         this.m.add(new He(this.i));
         this.m.add(new pK(this.i));
         this.m.add(new Ov(this.i));
         this.m.add(new la(this.i));
         this.m.add(new fz(this.i));
         this.m.add(new ED(this.i));
         this.m.add(new fd(this.i));
         this.m.add(new uk(this.i));
         this.m.add(new XJ(this.i));
         this.m.add(new Nb(this.i));
         this.m.add(new Yc(this.i));
         this.m.add(new oX(this.i));
         this.m.add(new TK(this.i));
         this.m.add(new LE(this.i, "whisper"));
         this.m.add(new LE(this.i, "w"));
         this.m.add(new Rk("reply", this.i));
         this.m.add(new Rk("r", this.i));
         this.m.add(new vr(this.i));
         this.m.add(new RB(this.i));
         this.m.add(new eu(this.i));
         this.m.add(new GU(this.i));
         this.m.add(new LY(this.i));
         this.m.add(new vb(this.i));
         this.m.add(new DS("exit", this.i));
         this.m.add(new DS("quit", this.i));
         this.m.add(new Zs(this.i));
         this.m.add(new KP(this.i));
         this.m.add(new EF(this.n));
         this.m.add(new FV(this.i));
         this.m.add(new ag(this.i));
         this.m.add(new Dx(this.i));
         this.m.add(new hM(this.i));
         this.m.add(new FK(this.i));
         Collections.sort(this.m, d);
      }

      this.n.addAll(this.j);
      this.n.addAll(this.k);
      this.n.addAll(this.m);
      Collections.sort(this.n, d);

      for (int var2 = 0; var2 < this.n.size(); ++var2) {
         lj var3 = (lj) this.n.get(var2);
         this.o.put(var3.f(), var3);
         this.C.add(var3.f().toLowerCase());
      }

   }

   public boolean b(String var1) {
      return this.C.contains(var1.toLowerCase());
   }

   public void b() {
      this.j.removeAll(this.l);
      this.k.removeAll(this.l);
      this.n.removeAll(this.l);

      for (int var1 = 0; var1 < this.l.size(); ++var1) {
         String var2 = ((Os) this.l.get(var1)).f();
         this.o.remove(var2);
         this.B.remove(var2);
      }

      this.l.clear();
   }

   public void a(List list) {
      this.b();

      // If we're a bot, don't load dynamic commands ._.
      if (this.i.D()) return;

      ArrayList newCmds = new ArrayList();
      Iterator i = list.iterator();
      while (i.hasNext()) {
         String json = (String) i.next();

         try {
            Os cmd = ub.a(this, json);
            newCmds.add(cmd);
         } catch (Exception var6) {
            if (!var6.getMessage().startsWith("Dynamic")) {
               a.warn("Failed to parse dynamic command: " + json, var6);
            }
         }
      }

      this.l.addAll(newCmds);

      Os var7;
      i = newCmds.iterator();

      while (i.hasNext()) {
         var7 = (Os) i.next();
         if (var7.g().contains(YY.d)) {
            this.j.add(var7);
         } else {
            this.k.add(var7);
         }

         this.n.add(var7);
         this.o.put(var7.f(), var7);
      }

      Collections.sort(this.j, d);
      Collections.sort(this.k, d);
      Collections.sort(this.n, d);
   }

   public lj[] c(String var1) {
      ArrayList var2 = new ArrayList();
      if (var1 == null) {
         for (int var3 = 0; var3 < this.n.size(); ++var3) {
            var2.add((lj) this.n.get(var3));
         }
      } else {
         String var6 = var1.toLowerCase();

         for (int var4 = 0; var4 < this.n.size(); ++var4) {
            String var5 = ((lj) this.n.get(var4)).f().toLowerCase();
            if (var6 == null || var5.contains(var6)) {
               var2.add((lj) this.n.get(var4));
            }
         }
      }

      return (lj[]) var2.toArray(new lj[var2.size()]);
   }

   public void a(hp var1) {
      this.q = var1;
      List var2 = var1.w();
      Iterator var4 = var2.iterator();

      while (var4.hasNext()) {
         Df var3 = (Df) var4.next();
         lj var5 = this.a(var3.a());
         if (var5 != null && var5 instanceof Os) {
            Os var6 = (Os) var5;
            var6.c(var3.b());
         }
      }

   }

   public List c() {
      ArrayList var1 = new ArrayList();
      Iterator var3 = this.j.iterator();

      Os var2;
      Df var4;
      while (var3.hasNext()) {
         var2 = (Os) var3.next();
         var4 = new Df();
         var4.a(var2.f());
         var4.a((Collection) var2.g());
         var1.add(var4);
      }

      var3 = this.k.iterator();

      while (var3.hasNext()) {
         var2 = (Os) var3.next();
         var4 = new Df();
         var4.a(var2.f());
         var4.a((Collection) var2.g());
         var1.add(var4);
      }

      return var1;
   }

   public FW a(Jt var1) {
      lY var2 = var1.f();
      UUID var3 = var2.f();
      FW var4 = (FW) this.p.get(var3);
      fN var5 = this.i.u().c();
      if (var4 == null) {
         if (var5 != null) {
            List var6 = var5.m().l();

            for (int var7 = 0; var7 < var6.size(); ++var7) {
               UUID var8 = (UUID) var6.get(var7);
               if (var8.equals(var2.D())) {
                  var4 = new FW(var2);
                  var4.a(YY.b);
                  this.p.put(var3, var4);
                  return var4;
               }
            }
         }

         var4 = new FW(var2);
         if (this.i.E()) {
            var4.a(YY.b);
         } else {
            var4.a(YY.c);
         }
      }

      return var4;
   }

   public FW b(Jt var1) {
      FW var2 = this.a(var1);
      if (var2.c() == YY.c) {
         var2.a(YY.d);
      }

      return var2;
   }

   public boolean a(FW var1, String var2) {
      if (this.q == null) {
         return false;
      } else if (this.c(var1) > 5) {
         return false;
      } else if (this.q.s() && this.q.z() != null && this.q.z().length() > 0 && this.q.z().equals(var2)) {
         var1.a(YY.b);
         this.p.put(var1.b(), var1);
         return true;
      } else {
         this.b(var1);
         return false;
      }
   }

   private void b(FW var1) {
      this.h();
      ArrayList var2 = (ArrayList) this.r.get(var1.b());
      if (var2 == null) {
         var2 = new ArrayList();
         this.r.put(var1.b(), var2);
      }

      var2.add(Long.valueOf(System.currentTimeMillis()));
   }

   private void h() {
      long var1 = System.currentTimeMillis();
      Iterator var3 = this.r.keySet().iterator();
      ArrayList var4 = new ArrayList();

      while (var3.hasNext()) {
         UUID var5 = (UUID) var3.next();
         ArrayList var6 = (ArrayList) this.r.get(var5);

         for (int var7 = 0; var7 < var6.size(); ++var7) {
            long var8 = ((Long) var6.get(var7)).longValue();
            if (var8 + 300000L < var1) {
               var6.remove(var7);
               --var7;
            }
         }

         if (var6.size() <= 0) {
            var4.add(var5);
         }
      }

      for (int var10 = 0; var10 < var4.size(); ++var10) {
         UUID var11 = (UUID) var4.get(var10);
         this.r.remove(var11);
      }

   }

   private int c(FW var1) {
      this.h();
      ArrayList var2 = (ArrayList) this.r.get(var1.b());
      return var2 == null ? 0 : var2.size();
   }

   public List d() {
      return this.j;
   }

   public boolean e() {
      return this.s;
   }

   public boolean a(FW var1, NB var2) {
      if (var1 == c) {
         return true;
      } else {
         Long var3 = (Long) this.t.get(var1.b());
         long var4 = System.currentTimeMillis();
         if (var3 != null && var3.longValue() > var4) {
            long var6 = var3.longValue() - var4;
            if (var6 > 0L) {
               var2.a(XY.a(var2.b()) + "You must wait " + me.a(var6, true) + " before calling another vote.", false);
               return false;
            }
         }

         return true;
      }
   }

   public void a(NB var1, Jt var2, FW var3, Os var4, String[] var5) {
      this.a(var1, var2, var3, var4, var5, 15000L, 2000L);
   }

   public void a(NB var1, Jt var2, FW var3, Os var4, String[] var5, long var6, long var8) {
      if (this.e()) {
         var1.a(XY.a(var1.b()) + var1.c() + "You cannot call a vote, there is one already in progress.", false);
      } else if (this.a(var3, var1)) {
         this.e = var6;
         this.f = var8;
         long now = System.currentTimeMillis();
         Iterator var12 = this.t.entrySet().iterator();

         while (var12.hasNext()) {
            Entry var13 = (Entry) var12.next();
            Long var14 = (Long) var13.getValue();
            if (var14.longValue() < now) {
               var12.remove();
            }
         }

         long cooldown = (long) mods.Config.get(this.q.a()).vote_cooldown;
         this.t.put(var3.b(), now + cooldown);
         this.A.clear();
         this.s = true;
         this.x = false;
         this.u = now;
         this.v = this.l();
         this.b = var4;
         this.w = var5;
         this.z = var3;
         Arrays.fill(this.h, 0);
         ai var16 = var4.a(this.z, var2, this.w);
         StringBuilder var17 = new StringBuilder();
         if (var2 != null) {
            var17.append(var2.h().e().a().E());
            var17.append(" has called a vote to ");
         }

         int var15;
         if (var16.a().size() > 0) {
            for (var15 = 0; var15 < var16.a().size(); ++var15) {
               var17.append((String) var16.a().get(var15));
            }

            var17.append(". ");
         }

         if (var16.b().size() > 0) {
            for (var15 = 0; var15 < var16.b().size(); ++var15) {
               var17.append((String) var16.b().get(var15));
            }

            var17.append(".");
         }

         this.g = var4.a(var5);
         this.h = new int[this.g.length];
         ML var18 = new ML(this.i, var16, var17.toString(), this.g, this.b.d(), this.b.e());
         this.i.u().c().l().a((kY) var18);
      }
   }

   public void f() {
      if (this.s) {
         long now = System.currentTimeMillis();
         if (!this.x && now > this.u + this.e) {
            this.i();
         }

         if (this.x && now > this.y + this.f) {
            if (this.j()) {
               this.b.a(new FZ(this), this.z, (Jt) null, this.h, this.w);
            } else {
               long cooldown = (long) mods.Config.get(this.q.a()).vote_fail_cooldown;
               this.t.put(this.z.b(), now + cooldown);
            }

            this.A.clear();
            this.s = false;
            this.x = false;
            this.u = 0L;
            this.b = null;
            this.w = null;
         }
      }

   }

   private void i() {
      if (!this.x) {
         this.x = true;
         this.y = System.currentTimeMillis();
         StringBuilder var1 = new StringBuilder();
         var1.append("Vote ");
         boolean var2 = this.j();
         if (var2) {
            var1.append("passed");
         } else {
            var1.append("failed");
         }

         int var3;
         int var4;
         if (this.g == Os.i) {
            var3 = Math.round(100.0F * (float) this.h[0] / (float) (this.h[0] + this.h[1]));
            var1.append(" with " + var3 + "% yes votes");
            if (var2) {
               var1.append(".");
            } else {
               var1.append(" (needed >" + (int) Math.round(100.0D * this.b.c()) + "%).");
            }
         } else {
            var3 = 0;
            var4 = 0;

            int var5;
            for (var5 = 0; var5 < this.h.length; ++var5) {
               var4 += this.h[var5];
               if (this.h[var5] > this.h[var3]) {
                  var3 = var5;
               }
            }

            var5 = Math.round(100.0F * (float) this.h[var3] / (float) var4);
            var1.append(" with " + var5 + "% " + this.g[var3] + " votes");
            if (var2) {
               var1.append(".");
            } else {
               var1.append(" (needed >" + (int) Math.round(100.0D * this.b.c()) + "%).");
            }
         }

         if (var2) {
            var1.append("\n> ");
            ai var6 = this.b.a(this.z, (Jt) null, this.w);

            for (var4 = 0; var4 < var6.a().size(); ++var4) {
               var1.append((String) var6.a().get(var4));
            }
         }

         OU var7 = new OU(this.i, var1.toString());
         this.i.u().c().l().a((kY) var7);
      }
   }

   private boolean j() {
      return this.b.a(this.h);
   }

   private void k() {
      boolean var1 = this.b.a(this.h, this.v);
      if (var1) {
         this.i();
      }

   }

   public boolean a(FW var1) {
      FW var2 = (FW) this.A.get(var1.b());
      return var2 != null;
   }

   public String a(Jt var1, FW var2, int var3) {
      if (this.a(var2)) {
         return "You have already cast a vote. You cannot vote again.";
      } else if (var3 <= this.g.length && var3 >= 0) {
         String var4 = var1.h().e().a().E() + " voted " + this.g[var3 - 1];
         si var5 = new si(this.i, var4, var3);
         this.i.u().c().l().a((kY) var5);
         this.A.put(var2.b(), var2);
         ++this.h[var3 - 1];
         this.k();
         return "Vote cast for option " + this.g[var3 - 1];
      } else {
         return "Invalid vote option.";
      }
   }

   private int l() {
      int var1 = 0;
      if (this.i.J() != null && this.i.J().u() != null && this.i.J().u().j() != null) {
         List var2 = this.i.J().u().j();

         for (int var3 = 0; var3 < var2.size(); ++var3) {
            if (!((mF) var2.get(var3)).e().a().D().equals(mF.a)) {
               ++var1;
            }
         }

         return var1;
      } else {
         return var1;
      }
   }

   public void a(String var1, NB var2) {
      this.B.put(var1, var2);
   }

   public NB d(String var1) {
      return (NB) this.B.remove(var1);
   }

   public List g() {
      return this.k;
   }
}
